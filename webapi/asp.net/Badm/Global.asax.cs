﻿using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Badm.Admin.DI;
using Ninject;
using Ninject.Web.Common.WebHost;
using Ninject.Web.WebApi;

namespace Badm
{
    public class WebApiApplication : NinjectHttpApplication //заменяем HttpApplication, чтобы использовать Ninject 
    {        
        protected override void OnApplicationStarted()
        {
            base.OnApplicationStarted();

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected override IKernel CreateKernel()
        {
            // регистрируем модули с внедрением
            var kernel = new StandardKernel(new QueryModule(), new IdentityModule());
            //RegisterServices(kernel);

            // для того, чтоб работало внедрение в контроллеры. DependencyResolver - глобальный объект (можно обращаться в любом месте кода DependencyResolver.Current.GetService<IUserManager>)
            GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);
            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private void RegisterServices(IKernel kernel)
        {
            
        }

    }
}
