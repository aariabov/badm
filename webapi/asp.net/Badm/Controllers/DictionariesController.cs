﻿using System.Collections.Generic;
using System.Web.Http;
using Badm.Admin.IQueryDAL.Common;
using Badm.Admin.Web.Models;
using Badm.Admin.Web.Resources;
using Badm.Controllers;
using Badm.Resources;
using Saule.Http;

namespace Badm.Admin.Web.Controllers
{
    [RoutePrefix("api")]
    [Authorize (Roles = "admin")]
    public class DictionariesController : BaseQueryController
    {
        //private readonly BadmContext _db = new BadmContext("BadmContext");

        
        public DictionariesController(IQueryUOW iQueryUOW) : base(iQueryUOW)
        {
            
        }

        

        //[HttpGet, Route("categories"), ReturnsResource(typeof(CategoryResource))]
        //public IEnumerable<CategoryDTO> GetCategories()
        //{
        //    var categories = _iQueryUOW.Categories.GetAll();
        //    return categories;
        //}

        //[HttpGet, Route("makers"), ReturnsResource(typeof(MakerResource))]
        //public IEnumerable<MakerDTO> GetMakers()
        //{
        //    var makers = _iQueryUOW.Makers.GetAll();
        //    return makers;
        //}

        //[HttpGet, Route("categories"), ReturnsResource(typeof(CategoryResource))]
        //public IQueryable<Category> GetCategories()
        //{
        //    return _db.Categories;
        //}

        //[HttpGet, Route("makers"), ReturnsResource(typeof(MakerResource))]
        //public IQueryable<Maker> GetMakers()
        //{
        //    return _db.Makers;
        //}

        //[HttpGet, Route("string-lenghts"), ReturnsResource(typeof(StringLenghtResource))]
        //public IEnumerable<StringLenghtDTO> GetStringLenghts()
        //{
        //    return _iQueryUOW.StringLenghts.GetAll();
        //}

        //[HttpGet, Route("grip-materials"), ReturnsResource(typeof(GripMaterialResource))]
        //public IQueryable<GripMaterial> GetGripMaterials()
        //{
        //    return _db.GripMaterials;
        //}

        //[HttpGet, Route("grip-types"), ReturnsResource(typeof(GripTypeResource))]
        //public IQueryable<GripType> GetGripTypes()
        //{
        //    return _db.GripTypes;
        //}

        //[HttpGet, Route("balances"), ReturnsResource(typeof(BalanceResource))]
        //public IQueryable<Balance> GetBalances()
        //{
        //    return _db.Balances;
        //}

        //[HttpGet, Route("flexes"), ReturnsResource(typeof(FlexResource))]
        //public IQueryable<Flex> GetFlexs()
        //{
        //    return _db.Flexes;
        //}

        //[HttpGet, Route("levels"), ReturnsResource(typeof(LevelResource))]
        //public IQueryable<Level> GetLevels()
        //{
        //    return _db.Levels;
        //}

        //[HttpGet, Route("racket-materials"), ReturnsResource(typeof(RacketMaterialResource))]
        //public IQueryable<RacketMaterial> GetRacketMaterials()
        //{
        //    return _db.RacketMaterials;
        //}
    }
}
