﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Badm.Admin.IQueryDAL.Common;
using Badm.Admin.Web.Resources;
using Saule.Http;

namespace Badm.Admin.Web.Controllers
{
    [RoutePrefix("api")]
    public class CommonController: BaseQueryController
    {
        public CommonController(IQueryUOW iQueryUow) : base(iQueryUow)
        {
        }

        /// <summary>
        /// Возвращает диапазоны range-фильтров, запрашивается при первом переходе на страницу фильтрации
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("filter-ranges"), ReturnsResource(typeof(FilterRangeResource))]
        public IEnumerable<FilterRange> GetStringFilters(string modelName)
        {
            var model = new Helpers.ConcreteModelHelper(_iQueryUOW).GetModelByName(modelName);
            var ranges = model.GetFilterRanges();
            return ranges;
        }
    }
}