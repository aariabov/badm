﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Badm.Admin.IQueryDAL.Common;
using Badm.Admin.IQueryDAL.Rackets;
using Badm.Admin.IQueryDAL.Strings;
using Badm.Resources;
using Saule.Http;

namespace Badm.Admin.Web.Controllers.Rackets
{
    [RoutePrefix("api")]
    public class RacketsQueryController : BaseQueryController
    {
        public RacketsQueryController(IQueryUOW iQueryUow) : base(iQueryUow)
        {
        }

        [HttpGet, Route("rackets"), ReturnsResource(typeof(RacketResource))]
        public IEnumerable<RacketDTO> GetRackets([FromUri] int[] visibility, [FromUri] int[] stock, [FromUri] int[] maker, [FromUri] int[] balance, [FromUri] int[] flex, [FromUri] int[] headMaterial, [FromUri] int[] shaftMaterial, [FromUri] int[] level, int weightMin = 0, int weightMax = 0, int lenghtMin = 0, int lenghtMax = 0, int priceMin = 0, int priceMax = 0, int page = 1, int perPage = 3, string sort = "Date")
        {
            IEnumerable<RacketDTO> rackets = _iQueryUOW.Rackets.Get(
                visibility: visibility, 
                stock: stock, 
                maker: maker, 
                balance: balance, 
                flex: flex, 
                headMaterial: headMaterial, 
                shaftMaterial: shaftMaterial, 
                level: level, 
                weightMin: weightMin, 
                weightMax: weightMax, 
                lenghtMin: lenghtMin, 
                lenghtMax: lenghtMax, 
                priceMin: priceMin, 
                priceMax: priceMax, 
                page: page, 
                perPage: perPage, 
                sort: sort);

            if (rackets.Count() > 0)
            {
                RacketsMeta meta = _iQueryUOW.Rackets.GetMeta(
                    visibility: visibility,
                    stock: stock,
                    maker: maker,
                    balance: balance,
                    flex: flex,
                    headMaterial: headMaterial,
                    shaftMaterial: shaftMaterial,
                    level: level,
                    weightMin: weightMin,
                    weightMax: weightMax,
                    lenghtMin: lenghtMin,
                    lenghtMax: lenghtMax,
                    priceMin: priceMin,
                    priceMax: priceMax);

                HttpContext.Current.Items["Meta"] = meta;
            }
            return rackets;
        }

    }
}