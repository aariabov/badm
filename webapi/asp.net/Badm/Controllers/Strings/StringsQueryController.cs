﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Badm.Admin.IQueryDAL;
using Badm.Admin.IQueryDAL.Common;
using Badm.Admin.IQueryDAL.Strings;
using Badm.Admin.Web.Models;
using Badm.Admin.Web.Resources;
using Saule.Http;

namespace Badm.Admin.Web.Controllers.Strings
{
    [RoutePrefix("api")]    
    public class StringsQueryController : BaseQueryController
    {
        public StringsQueryController(IQueryUOW iQueryUow) : base(iQueryUow){}

        [HttpGet, Route("strings"), ReturnsResource(typeof(StringResource))]
        public IEnumerable<StringDTO> GetStrings([FromUri] int[] visibility, [FromUri] int[] stock, [FromUri] int[] maker, [FromUri(Name = "string-lenght")] int[] stringLenght, decimal gaugeMin = 0, decimal gaugeMax = 0, int priceMin = 0, int priceMax = 0, int page = 1, int perPage = 3, string sort = "Date")
        {
            IEnumerable<StringDTO> strings = _iQueryUOW.Strings.Get(
                visibility: visibility, 
                stock: stock, 
                maker: maker, 
                stringLenght: stringLenght, 
                gaugeMin: gaugeMin, 
                gaugeMax: gaugeMax, 
                priceMin: priceMin, 
                priceMax: priceMax, 
                page: page, 
                perPage: perPage, 
                sort: sort);

            if (strings.Count() > 0)
            {
                StringsMeta meta = _iQueryUOW.Strings.GetMeta(
                    visibility: visibility, 
                    stock: stock, 
                    maker: maker, 
                    stringLenght: stringLenght, 
                    gaugeMin: gaugeMin, 
                    gaugeMax: gaugeMax, 
                    priceMin: priceMin, 
                    priceMax: priceMax);

                HttpContext.Current.Items["Meta"] = meta;
            }

            return strings;
        }

    }
}
