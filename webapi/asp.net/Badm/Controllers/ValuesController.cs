﻿using System.Collections.Generic;
using System.Web.Http;
using Badm.Admin.IQueryDAL.Common;
using Badm.Admin.Web.Models;
using Badm.Admin.Web.Resources;
using Badm.Controllers;
using Badm.Resources;
using Saule.Http;

namespace Badm.Admin.Web.Controllers
{
    
    public class ValuesController : BaseQueryController
    {
        
        public ValuesController(IQueryUOW iQueryUow) : base(iQueryUow)
        {
        }

        

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
