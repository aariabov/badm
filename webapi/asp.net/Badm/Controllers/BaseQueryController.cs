﻿using System.Web.Http;
using Badm.Admin.IQueryDAL.Common;

namespace Badm.Admin.Web.Controllers
{
    //[RoutePrefix("api")] - какие то извраты, чтобы унаследовать этот атрибут
    public abstract class BaseQueryController : ApiController
    {
        protected readonly IQueryUOW _iQueryUOW;

        protected BaseQueryController(IQueryUOW iQueryUow)
        {
            _iQueryUOW = iQueryUow;
        }
    }
}
