﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Badm.Admin.IQueryDAL.Common;
using Badm.Admin.Web.Controllers;
using Badm.Models;
using Badm.Resources;
using Saule.Http;
using String = System.String;

namespace Badm.Controllers
{
    [RoutePrefix("api/mdls")]
    [ReturnsResource(typeof(MdlResource))]
    public class MdlsQueryController : BaseQueryController
    {
        //private readonly BadmContext _db = new BadmContext("BadmContext");

        public MdlsQueryController(IQueryUOW iQueryUow) : base(iQueryUow)
        {

        }

        //[HttpGet]
        //public IQueryable<Model> GetMdls(int page, int perPage, [FromUri] int[] makers, string sort = "name")
        //{
        //    var query = _db.Models.OfType<Model>().Include(m => m.Products).Where(m => m.CategoryId == 8);
        //    if (makers.Length != 0)
        //    {
        //        query = query.Where(m => makers.Contains(m.MakerId));
        //    }
        //    switch (sort)
        //    {
        //        case "name":
        //            query = query.OrderBy(s => s.Name);
        //            break;
        //        case "-name":
        //            query = query.OrderByDescending(s => s.Name);
        //            break;
        //        case "id":
        //            query = query.OrderBy(s => s.Id);
        //            break;
        //        case "-id":
        //            query = query.OrderByDescending(s => s.Id);
        //            break;
        //    }

        //    return query.Skip((page - 1) * perPage).Take(perPage);
        //}
    }
}
