﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Badm.Admin.IQueryDAL.DTOs;
using Badm.Admin.IQueryDAL.Interfaces;
using Badm.Admin.Web.Models;
using Badm.Admin.Web.Resources;
using Saule.Http;

namespace Badm.Admin.Web.Controllers
{
    public class TestController : BaseQueryController
    {
        public TestController(IQueryUOW iQueryUow) : base(iQueryUow)
        {
        }

        [HttpGet, Route("string-dicts"), ReturnsResource(typeof(StringDictsResource))]
        public StringDictsVM Get()
        {            
            //string yourJson = "{\"data\":[{\"type\":\"maker\",\"id\":\"11\",\"attributes\":{\"name\":\"Babolat\"}},{\"type\":\"maker\",\"id\":\"12\",\"attributes\":{\"name\":\"Yonex\"}},{\"type\":\"string-lenght\",\"id\":\"33\",\"attributes\":{\"name\":\"sdfsafdasd\"}}]}";
            
            //var response = this.Request.CreateResponse(HttpStatusCode.OK);
            //response.Content = new StringContent(yourJson, Encoding.UTF8, "application/json");
            //return response;

            StringDictsVM sw=new StringDictsVM
            {
                Id = 1,
                Makers = _iQueryUOW.Makers.GetAll(),
                StringLenghts = _iQueryUOW.StringLenghts.GetAll()
            };

            return sw;

        }
    }
}
