﻿using System;
using System.Linq;
using System.Web;
using Badm.Admin.IQueryDAL.Rackets;
using Badm.Admin.Web.Resources;
using Saule;

namespace Badm.Resources
{
    public class RacketResource : ApiResource
    {
        public RacketResource()
        {
            Attribute(nameof(RacketDTO.Name));
            Attribute(nameof(RacketDTO.CategoryId));
            Attribute(nameof(RacketDTO.MakerId));
            Attribute(nameof(RacketDTO.Description));
            Attribute(nameof(RacketDTO.ShaftMaterialId));
            Attribute(nameof(RacketDTO.HeadMaterialId));
            Attribute(nameof(RacketDTO.BalanceId));
            Attribute(nameof(RacketDTO.FlexId));
            Attribute(nameof(RacketDTO.LevelId));
            Attribute(nameof(RacketDTO.Lenght));
            Attribute(nameof(RacketDTO.Weight));
            Attribute(nameof(RacketDTO.IsWithString));

            HasMany<ProductResource>(nameof(RacketDTO.Products), "", LinkType.None);

            WithLinks(LinkType.None);
        }

        public override object GetMetadata(object response, Type resourceType, bool isEnumerable)
        {
            return HttpContext.Current.Items["Meta"];
        }

    }
}