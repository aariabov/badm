﻿using Badm.Admin.IQueryDAL.Common;
using Saule;

namespace Badm.Admin.Web.Resources
{
    public class ProductResource : ApiResource
    {
        public ProductResource()
        {
            Attribute(nameof(ProductDTO.ActualPrice));
            Attribute(nameof(ProductDTO.OldPrice));
            Attribute(nameof(ProductDTO.LittleImg));
            Attribute(nameof(ProductDTO.Count));
            Attribute(nameof(ProductDTO.Date));
            Attribute(nameof(ProductDTO.IsHit));
            Attribute(nameof(ProductDTO.IsInteres));
            Attribute(nameof(ProductDTO.IsNew));
            Attribute(nameof(ProductDTO.IsOrder));
            Attribute(nameof(ProductDTO.IsPromo));
            Attribute(nameof(ProductDTO.IsVisible));
            Attribute(nameof(ProductDTO.CountCheckOut));

            WithLinks(LinkType.None);
        }
    }
}