﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Badm.Admin.IQueryDAL.Common;
using Badm.Admin.Web.Models;
using Badm.Resources;
using Saule;

namespace Badm.Admin.Web.Resources
{
    public class FilterRangeResource1 : ApiResource
    {
        
        public FilterRangeResource1()
        {
            OfType("filter-range");
            //HasMany<MakerResource>(nameof(StringFilterRangesVM.Makers), "", LinkType.None);
            //HasMany<StringLenghtResource>(nameof(StringFilterRangesVM.StringLenghts), "", LinkType.None);
            Attribute(nameof(FilterRange.Min));
            Attribute(nameof(FilterRange.Max));
            //HasMany<FilterRangeResource>(nameof(StringFilterRangesVM.FilterRanges), "", LinkType.None);

            WithLinks(LinkType.None);
        }
    }
}