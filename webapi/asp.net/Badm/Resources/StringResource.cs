﻿using System;
using System.Web;
using Badm.Admin.IQueryDAL.Strings;
using Badm.Resources;
using Saule;

namespace Badm.Admin.Web.Resources
{
    public class StringResource : ApiResource
    {
        public StringResource()
        {
            Attribute(nameof(StringDTO.Name));
            Attribute(nameof(StringDTO.CategoryId));
            Attribute(nameof(StringDTO.MakerId));
            Attribute(nameof(StringDTO.Description));
            Attribute(nameof(StringDTO.StringLenghtId));
            Attribute(nameof(StringDTO.Gauge));

            HasMany<ProductResource>(nameof(StringDTO.Products), "", LinkType.None);

            WithLinks(LinkType.None);
        }

        public override object GetMetadata(object response, Type resourceType, bool isEnumerable)
        {
            return HttpContext.Current.Items["Meta"];
        }
    }
}