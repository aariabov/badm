﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Badm.Admin.IQueryDAL;
using Badm.Admin.IQueryDAL.Common;
using Saule;

namespace Badm.Admin.Web.Resources
{
    public class FilterRangeResource : ApiResource
    {
        public FilterRangeResource()
        {
            Attribute(nameof(FilterRange.Min));
            Attribute(nameof(FilterRange.Max));
            WithLinks(LinkType.None);
        }
    }
}