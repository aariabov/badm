﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Badm.Admin.IQueryDAL.Common;

namespace Badm.Admin.Web.Helpers
{
    /// <summary>
    /// Определяет модель по названию
    /// </summary>
    public class ConcreteModelHelper
    {
        private readonly IQueryUOW _iQueryUOW;

        public ConcreteModelHelper(IQueryUOW iQueryUow)
        {
            _iQueryUOW = iQueryUow;
        }

        public IModels GetModelByName(string modelName)
        {
            switch (modelName)
            {
                case "string":
                    return _iQueryUOW.Strings;
                case "other":
                    return _iQueryUOW.Others;
                case "racket":
                    return _iQueryUOW.Rackets;
                default:
                    new Exception($"Не найдена модель по имени {modelName}");
                    break;
            }
            return null;
        }
    }
}