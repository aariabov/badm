﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Saule;
using Saule.Http;

namespace Badm
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.

            //config.Formatters.Remove(config.Formatters.XmlFormatter);
            //config.Formatters.Remove(config.Formatters.JsonFormatter);

            // Web API routes
            config.MapHttpAttributeRoutes();
            
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // доступ только для админов
            //var authorize = new AuthorizeAttribute();
            //authorize.Roles = "admin";
            //config.Filters.Add(authorize);

            //config.ConfigureJsonApi(new JsonApiConfiguration());
            config.ConfigureJsonApi();

            // Для Hypermedia
            //var resolver = CreateResolver();
            //config.Formatters.Add(new JsonApiMediaTypeFormatter(resolver));
            //config.Formatters.Add(new JsonApiMetadataMediaTypeFormatter(resolver));
        }

        // Для Hypermedia
        //static IContractResolver CreateResolver()
        //{
        //    return new Builder()
        //        .With<CategoryVM>("categories")
        //            .Id(nameof(CategoryVM.Id))
        //        .With<MakerVM>("makers")
        //            .Id(nameof(MakerVM.Id))
        //        .With<StringLenghtVM>("string-lenghts")
        //            .Id(nameof(StringLenghtVM.Id))
        //        .With<GripMaterialVM>("grip-materials")
        //            .Id(nameof(GripMaterialVM.Id))
        //        .With<GripTypeVM>("grip-types")
        //            .Id(nameof(GripTypeVM.Id))
        //        .With<BalanceVM>("balances")
        //            .Id(nameof(BalanceVM.Id))
        //        .With<FlexVM>("flexes")
        //            .Id(nameof(FlexVM.Id))
        //        .With<LevelVM>("levels")
        //            .Id(nameof(LevelVM.Id))
        //        .With<RacketMaterialVM>("racket-materials")
        //            .Id(nameof(RacketMaterialVM.Id))
        //        .With<ModelVM>("mdls")
        //            .Id(nameof(ModelVM.Id))
        //            //.BelongsTo<StringVM>(nameof(ModelVM.String))
        //            .HasMany<ProductVM>(nameof(ModelVM.Products))
        //        .With<ProductVM>("products")
        //            .Id(nameof(ProductVM.Id))
        //        .With<StringVM>("strings")
        //            .Id(nameof(StringVM.Id))
        //            .HasMany<ProductVM>(nameof(StringVM.Products))
        //        .With<GripVM>("grips")
        //            .Id(nameof(GripVM.Id))
        //            .HasMany<ProductVM>(nameof(StringVM.Products))
        //        .With<RacketVM>("rackets")
        //            .Id(nameof(RacketVM.Id))
        //            .HasMany<ProductVM>(nameof(RacketVM.Products))
        //        .Build();
        //}
    }

    

    
}
