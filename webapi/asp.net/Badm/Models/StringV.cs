﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Badm.Models
{
    public class StringV
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public int MakerId { get; set; }
        public string Description { get; set; }
        public int StringLenghtId { get; set; }
        public decimal Gauge { get; set; }

        public IEnumerable<ProductV> Products { get; set; }
    }
}