﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Badm.Admin.IQueryDAL;
using Badm.Admin.IQueryDAL.Common;

namespace Badm.Admin.Web.Models
{
    public class StringFilterRangesVM
    {
        public string Id;
        public IEnumerable<FilterRange> FilterRanges;

        public StringFilterRangesVM(IEnumerable<FilterRange> filterRanges)
        {
            Id = "string";
            FilterRanges = filterRanges;
        }
    }
}