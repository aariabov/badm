﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Badm.Admin.IQueryDAL.Common;
using Badm.Admin.IQueryDAL.EF;
using Ninject.Modules;

namespace Badm.Admin.DI
{
    public class QueryModule : NinjectModule
    {
        public override void Load()
        {
            // внедряем провайдера, который будет отвечать за запросы
            Bind<IQueryUOW>().To<QueryUOWEF>().WithConstructorArgument("connectionString", "BadmContext");
        }
    }
}
