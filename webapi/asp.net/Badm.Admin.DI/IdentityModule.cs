﻿using Badm.IIdentity;
using Badm.IIdentity.EF;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.Owin.Security.OAuth;
using Ninject;
using Ninject.Modules;

namespace Badm.Admin.DI
{
    public class IdentityModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IdentityContext>().ToSelf().WithConstructorArgument("connectionString", "IdentityContext");
            Bind<IUserStore<AppUser>>().To<UserStore<AppUser>>().WithConstructorArgument("context", KernelInstance.Get<IdentityContext>());
            Bind<IOAuthAuthorizationServerProvider>().To<ApplicationOAuthProvider>().WithConstructorArgument("publicClientId", "self");
            Bind<IDataProtectionProvider>()
                .To<DpapiDataProtectionProvider>()
                .WithConstructorArgument("ApplicationName");
            Bind<IUserManager>().To<AppUserManager>();
        }
    }
}
