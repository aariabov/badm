﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Badm.IIdentity.EF
{
    public class IdentityContext : IdentityDbContext<AppUser>
    {
        public IdentityContext(string connectionString)
            : base(connectionString, throwIfV1Schema: false)
        {
        }
    }
}
