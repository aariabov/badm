﻿using System.Collections.Generic;
using Badm.Admin.IQueryDAL.Common;
using Badm.Admin.IQueryDAL.Strings;

namespace Badm.Admin.IQueryDAL.Others
{
    public interface IOthers : IModels
    {
        IEnumerable<OtherDTO> Get(int[] visibility, int[] stock, int[] maker, int page, int perPage, int priceMin = 0, int priceMax = 0, string sort = "date");
        StringsMeta GetMeta(int[] visibility, int[] stock, int[] maker, int priceMin = 0, int priceMax = 0);
    }
}
