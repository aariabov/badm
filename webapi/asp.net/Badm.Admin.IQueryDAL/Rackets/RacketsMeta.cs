﻿using System.Collections.Generic;
using Badm.Admin.IQueryDAL.Common;

namespace Badm.Admin.IQueryDAL.Rackets
{
    public class RacketsMeta : ModelsMeta
    {
        public MetaRange Weight;
        public MetaRange Lenght;
        public Dictionary<int, int> Balance;
        public Dictionary<int, int> Flex;
        public Dictionary<int, int> Level;
        public Dictionary<int, int> HeadMaterial;
        public Dictionary<int, int> ShaftMaterial;

        public RacketsMeta(Dictionary<int, int> visibility, Dictionary<int, int> stock, Dictionary<int, int> maker, MetaRange price, int totalCount, Dictionary<int, int> level, Dictionary<int, int> balance, Dictionary<int, int> flex, Dictionary<int, int> shaftMaterial, Dictionary<int, int> headMaterial, MetaRange weight, MetaRange lenght) : base(visibility, stock, maker, price, totalCount)
        {
            Level = level;
            Balance = balance;
            Flex = flex;
            ShaftMaterial = shaftMaterial;
            HeadMaterial = headMaterial;
            Weight = weight;
            Lenght = lenght;
        }
    }
}
