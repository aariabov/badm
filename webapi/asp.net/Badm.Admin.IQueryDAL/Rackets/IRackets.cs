﻿using System.Collections.Generic;
using Badm.Admin.IQueryDAL.Common;
using Badm.Admin.IQueryDAL.Strings;

namespace Badm.Admin.IQueryDAL.Rackets
{
    public interface IRackets : IModels
    {
        IEnumerable<RacketDTO> Get(int[] visibility, int[] stock, int[] maker, int[] balance, int[] flex, int[] headMaterial, int[] shaftMaterial, int[] level, int weightMin, int weightMax, int lenghtMin, int lenghtMax, int priceMin, int priceMax, int page, int perPage, string sort);
        RacketsMeta GetMeta(int[] visibility, int[] stock, int[] maker, int[] balance, int[] flex, int[] headMaterial, int[] shaftMaterial, int[] level, int weightMin = 0, int weightMax = 0, int lenghtMin = 0, int lenghtMax = 0, int priceMin = 0, int priceMax = 0);
    }
}
