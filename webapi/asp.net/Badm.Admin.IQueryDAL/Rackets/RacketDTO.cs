﻿using Badm.Admin.IQueryDAL.Common;

namespace Badm.Admin.IQueryDAL.Rackets
{
    public class RacketDTO : ModelDTO
    {
        public int Weight { get; set; }
        public int Lenght { get; set; }
        public int BalanceId { get; set; }
        public int FlexId { get; set; }
        public int HeadMaterialId { get; set; }
        public int LevelId { get; set; }
        public int ShaftMaterialId { get; set; }
        public bool IsWithString { get; set; }
    }
}
