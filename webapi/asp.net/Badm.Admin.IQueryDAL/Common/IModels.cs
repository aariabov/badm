﻿using System.Collections.Generic;

namespace Badm.Admin.IQueryDAL.Common
{
    public interface IModels
    {
        IEnumerable<FilterRange> GetFilterRanges();
    }
}
