﻿using System.Collections.Generic;

namespace Badm.Admin.IQueryDAL.Common
{
    public abstract class ModelDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public int MakerId { get; set; }
        public string Description { get; set; }

        public IEnumerable<ProductDTO> Products { get; set; }
    }
}
