﻿using System;

namespace Badm.Admin.IQueryDAL.Common
{
    /// <summary>
    /// Диапазон для фильтра, который передается в метаданных. Учитываются другие фильтры, поэтому этот диапазон может быть меньше чем максимальный.
    /// </summary>
    public class MetaRange
    {
        public ValueType Min;
        public ValueType Max;

        public MetaRange(ValueType min, ValueType max)
        {
            Min = min;
            Max = max;
        }
    }
}
