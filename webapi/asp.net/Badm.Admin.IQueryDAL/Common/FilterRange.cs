﻿using System;

namespace Badm.Admin.IQueryDAL.Common
{
    /// <summary>
    /// Максимальный диапазон для фильтра, где Id - название фильтра
    /// </summary>
    public struct FilterRange
    {
        public string Id;
        public ValueType Min;
        public ValueType Max;

        //public FilterRange(string id, ValueType min, ValueType max)
        //{
        //    Id = id;
        //    Min = min;
        //    Max = max;
        //}

        public FilterRange(string id, MetaRange range)
        {
            Id = id;
            Min = range.Min;
            Max = range.Max;
        }
    }
}
