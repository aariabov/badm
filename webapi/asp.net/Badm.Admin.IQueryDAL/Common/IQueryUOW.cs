﻿using System;
using Badm.Admin.IQueryDAL.Others;
using Badm.Admin.IQueryDAL.Rackets;
using Badm.Admin.IQueryDAL.Strings;

namespace Badm.Admin.IQueryDAL.Common
{
    public interface IQueryUOW : IDisposable
    {
        IStrings Strings { get; }
        IRackets Rackets { get; }
        IOthers Others { get; }
    }
}
