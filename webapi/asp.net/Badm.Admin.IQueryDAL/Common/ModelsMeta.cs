﻿using System.Collections.Generic;

namespace Badm.Admin.IQueryDAL.Common
{
    public abstract class ModelsMeta
    {
        public int TotalCount;
        public MetaRange Price;
        public Dictionary<int, int> Visibility;
        public Dictionary<int, int> Maker;
        public Dictionary<int, int> Stock;

        public ModelsMeta(Dictionary<int, int> visibility, Dictionary<int, int> stock, Dictionary<int, int> maker, MetaRange price, int totalCount)
        {
            TotalCount = totalCount;
            Price = price;
            Visibility = visibility;
            Maker = maker;
            Stock = stock;
        }
    }
}
