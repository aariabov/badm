﻿namespace Badm.Admin.IQueryDAL.Common
{
    public class ProductDTO
    {
        public int Id { get; set; }
        public int? OldPrice { get; set; }
        public System.DateTime Date { get; set; }
        public string Colors { get; set; }
        public int ActualPrice { get; set; }
        public string LittleImg { get; set; }
        public string BigImg { get; set; }
        public int Count { get; set; }
        public int ModelId { get; set; }
        public bool IsVisible { get; set; }
        public bool IsHit { get; set; }
        public bool IsNew { get; set; }
        public bool IsPromo { get; set; }
        public bool IsInteres { get; set; }
        public bool IsOrder { get; set; }

        public int CountCheckOut { get; set; }
    }
}
