﻿using System.Collections.Generic;
using Badm.Admin.IQueryDAL.Common;

namespace Badm.Admin.IQueryDAL.Strings
{
    public interface IStrings : IModels
    {
        IEnumerable<StringDTO> Get(int[] visibility, int[] stock, int[] maker, int[] stringLenght, decimal gaugeMin, decimal gaugeMax, int priceMin, int priceMax, int page, int perPage, string sort);
        StringsMeta GetMeta(int[] visibility, int[] stock, int[] maker, int[] stringLenght, decimal gaugeMin, decimal gaugeMax, int priceMin, int priceMax);
    }
}
