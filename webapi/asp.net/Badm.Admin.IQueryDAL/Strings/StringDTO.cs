﻿using Badm.Admin.IQueryDAL.Common;

namespace Badm.Admin.IQueryDAL.Strings
{
    public class StringDTO : ModelDTO
    {
        public int StringLenghtId { get; set; }
        public decimal Gauge { get; set; }
    }
}
