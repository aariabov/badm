﻿using System.Collections.Generic;
using Badm.Admin.IQueryDAL.Common;

namespace Badm.Admin.IQueryDAL.Strings
{
    public class StringsMeta : ModelsMeta
    {
        public MetaRange Gauge;
        public Dictionary<int, int> StringLenght;

        public StringsMeta(Dictionary<int, int> visibility, Dictionary<int, int> stock, Dictionary<int, int> maker, MetaRange price, int totalCount, Dictionary<int, int> stringLenght, MetaRange gauge) : base(visibility, stock, maker, price, totalCount)
        {
            Gauge = gauge;
            StringLenght = stringLenght;
        }
    }
}
