﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Badm.Admin.IQueryDAL.Common;
using System.Linq.Dynamic;

namespace Badm.Admin.IQueryDAL.EF
{


    abstract class ModelsEF
    {
        protected BadmContext _db;

        protected ModelsEF(BadmContext db)
        {
            _db = db;
        }

        /// <summary>
        /// Главный запрос: фильтрует, сортирует и делает маппинг на DTO
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TModelDTO"></typeparam>
        /// <param name="models"></param>
        /// <param name="visibility"></param>
        /// <param name="stock"></param>
        /// <param name="maker"></param>
        /// <param name="priceMin"></param>
        /// <param name="priceMax"></param>
        /// <param name="page"></param>
        /// <param name="perPage"></param>
        /// <param name="sort"></param>
        /// <returns></returns>
        protected IEnumerable<TModelDTO> GetModels<TModel, TModelDTO>(IQueryable<TModel> models, int[] visibility, int[] stock, int[] maker, int priceMin, int priceMax, int page, int perPage, string sort) where TModel : Model where TModelDTO : ModelDTO
        {
            var productFilterCondition = ProductFilterCondition(visibility, stock, priceMin, priceMax);

            Expression<Func<TModel, IQueryable<Product>>> productsExpr = null;

            switch (sort)
            {
                case "Date":
                    models = models.OrderBy(m => m.Products.Max(p => p.Date));
                    productsExpr = m => m.Products.AsQueryable().Where(productFilterCondition).OrderBy(p => p.Date);
                    break;
                case "-Date":
                    models = models.OrderByDescending(m => m.Products.Max(p => p.Date));
                    productsExpr = m => m.Products.AsQueryable().Where(productFilterCondition).OrderByDescending(p => p.Date);
                    break;
                case "ActualPrice":
                    models = models.OrderBy(m => m.Products.Max(p => p.ActualPrice));
                    productsExpr = m => m.Products.AsQueryable().Where(productFilterCondition).OrderBy(p => p.ActualPrice);
                    break;
                case "-ActualPrice":
                    models = models.OrderByDescending(m => m.Products.Max(p => p.ActualPrice));
                    productsExpr = m => m.Products.AsQueryable().Where(productFilterCondition).OrderByDescending(p => p.ActualPrice);
                    break;
            }

            // конфиг для маппера
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Product, ProductDTO>().ForMember(dest => dest.CountCheckOut, opt => opt.MapFrom(p => _db.CheckOuts.Where(c => c.ProductId == p.Id).Select(c => c.Count).DefaultIfEmpty(0).Sum()));
                cfg.CreateMap<TModel, TModelDTO>().ForMember(dest => dest.Products, opt => opt.MapFrom(productsExpr));
            });

            var results = models.Skip((page - 1) * perPage).Take(perPage).ProjectTo<TModelDTO>(config).ToList();

            return results;
        }

        //private Expression<Func<TModel, int>> ModelSortExpression<TModel>(string sort) where TModel : Model
        //{
        //    return m => m.Products.Max(p => p.ActualPrice);
        //}

        //protected IEnumerable<TModelDTO> Get<TModel, TModelDTO>(IQueryable<TModel> models, int[] visibility, int[] stock, int[] maker, int page, int perPage, int priceMin = 0, int priceMax = 0, string sort = "date") where TModel : Model where TModelDTO : ModelDTO
        //{
        //    // информация по количеству на чекауте
        //    var checkOutGroup = _db.CheckOuts.GroupBy(g => g.ProductId).Select(g => new { ProductId = g.Key, CountCheckOut = g.Sum(c => c.Count) });

        //    var productFilterCondition = ProductFilterCondition(visibility, stock, priceMin, priceMax);

        //    // отфильтрованные модели + их отфильтрованные продукты + информация о наличии
        //    var results = models.Select(model => new 
        //    {
        //        model,
        //        products =
        //            from p in model.Products.AsQueryable().Where(productFilterCondition)
        //            join c in checkOutGroup on p.Id equals c.ProductId into ps
        //            from lj in ps.DefaultIfEmpty()
        //            let CountCheckOut = lj == null ? 0 : lj.CountCheckOut
        //            select new{p, CountCheckOut}
        //            });

        //    string sortPropertyName = "";
        //    results = results.OrderBy(m => m.model.Id);
        //    results = results.Skip((page - 1) * perPage).Take(perPage);

        //    var config = new MapperConfiguration(cfg =>
        //    {
        //        cfg.CreateMap<Product, ProductDTO>();
        //        cfg.CreateMap<String, StringDTO>().ForMember(dest => dest.Products, opt => opt.Ignore());
        //    });
        //    var mapper = config.CreateMapper();

        //    List<TModelDTO> strings = new List<TModelDTO>();
        //    var modelsWithProducts = results.AsEnumerable().Distinct().ToList();
        //    foreach (var model in modelsWithProducts)
        //    {
        //        TModelDTO str = mapper.Map<TModelDTO>(model.model);
        //        List<ProductDTO> productDTOs = new List<ProductDTO>();
        //        foreach (var product in model.products)
        //        {
        //            ProductDTO productDto = mapper.Map<ProductDTO>(product.p);
        //            productDto.CountCheckOut = product.CountCheckOut;
        //            productDTOs.Add(productDto);
        //        }
        //        str.Products = productDTOs;
        //        strings.Add(str);
        //    }

        //    return strings;
        //}

        /// <summary>
        /// Диапазон для заданного параметра
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TType"></typeparam>
        /// <param name="entities"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        protected MetaRange GetMetaRange<TEntity, TType>(IQueryable<TEntity> entities, Expression<Func<TEntity, TType>> predicate) where TEntity : class where TType : struct
        {
            var res = entities
                .GroupBy(e => 1)
                .Select(g => new
                {
                    min = g.AsQueryable().Min(predicate),
                    max = g.AsQueryable().Max(predicate)
                }).ToList();

            // защита на случай, если не оказалось продуктов
            if (res.Count == 0)
            {
                return null;
            }
            else
            {
                var range = res.First();
                return new MetaRange(range.min, range.max);
            }
        }


        protected Dictionary<int, int> GetStockMeta(IQueryable<Product> products)
        {
            // информация по количеству на чекауте
            //var checkOutGroup = _db.CheckOuts.GroupBy(g => g.ProductId).Select(g => new { productId = g.Key, sum = g.Sum(c => c.Count) });
            //p => _db.CheckOuts.Where(c => c.ProductId == p.Id).Select(c => c.Count).DefaultIfEmpty(0).Sum())
            var productStock =
                from p in products
                let checkOutCount = _db.CheckOuts.Where(c => c.ProductId == p.Id).Select(c => c.Count).DefaultIfEmpty(0).Sum()
                select new
                {
                    StockId = p.Count > checkOutCount ? 1 : (p.IsOrder == true ? 2 : 3)
                };

            // информация о наличии продукта: 1 - в наличии, 2 - под заказ, 3 - нет в наличии
            //var productStock =
            //    from p in products
            //    join c in checkOutGroup on p.Id equals c.productId into pc
            //    from lj in pc.DefaultIfEmpty()
            //    let checkOutCount = lj == null ? 0 : lj.sum
            //    select new
            //    {
            //        StockId = p.Count > checkOutCount ? 1 : (p.IsOrder == true ? 2 : 3)
            //    };

            // запрос, возможно пустые значения
            Dictionary<int, int> stockCount = productStock.GroupBy(p => p.StockId).ToDictionary(g => g.Key, g => g.Count());

            Dictionary<int, int> res = new Dictionary<int, int>();
            res.Add(1, stockCount.ContainsKey(1) ? stockCount[1] : 0);
            res.Add(2, stockCount.ContainsKey(2) ? stockCount[2] : 0);
            res.Add(3, stockCount.ContainsKey(3) ? stockCount[3] : 0);

            return res;
        }

        //protected Dictionary<int, int> GetMakerMeta(IQueryable<Model> models, int[] visibility, int[] stock, int[] stringLenght, decimal gaugeMin, decimal gaugeMax, int priceMin, int priceMax)
        //{
        //    // отфильтрованные продукты
        //    IQueryable<Product> products = FilterProduct(models, visibility, stock, priceMin, priceMax);

        //    // нужны только продукты, информацию о производители можно получить из свойства
        //    // делаем left join всех производителей с производителями продуктов, и получаем по каждому производителю кол-во продуктов
        //    var makersCount =
        //        (from maker in _db.Makers
        //         join g in (from product in products
        //                    group product by product.Model.MakerId)
        //         on maker.Id equals g.Key into outer
        //         from res in outer.DefaultIfEmpty()
        //         select new
        //         {
        //             Id = maker.Id,
        //             Count = res.Count()
        //         }).ToDictionary(t => t.Id, t => t.Count);

        //    return makersCount;
        //}

        /// <summary>
        /// Информация о количестве продуктов для справочника
        /// </summary>
        /// <param name="models">Уже отфильтрованные модели</param>
        /// <param name="dict">Справочник</param>
        /// <param name="groupExpr">Выражение, по которому будет группировка</param>
        /// <param name="visibility">Видимость, для фильтрации продуктов</param>
        /// <param name="stock">Наличие, для фильтрации продуктов</param>
        /// <param name="priceMin">Мин цена, для фильтрации продуктов</param>
        /// <param name="priceMax">Макс цена, для фильтрации продуктов</param>
        /// <returns></returns>
        protected Dictionary<int, int> GetMetaDictionary(IQueryable<IDict> dict, IQueryable<IGrouping<int, Product>> groupedProducts)
        {
            // пример для производителей
            // нужны только продукты, информацию о производителе можно получить из свойства
            // делаем left join всех производителей с производителями продуктов, и получаем по каждому производителю кол-во продуктов
            var dictCount =
                (from d in dict
                 join g in groupedProducts
                 on d.Id equals g.Key into outer
                 from res in outer.DefaultIfEmpty()
                 select new
                 {
                     Id = d.Id,
                     Count = res.Count()
                 }).ToDictionary(t => t.Id, t => t.Count);

            return dictCount;
        }

        protected Dictionary<int, int> GetMetaBool(IQueryable<IGrouping<bool, Product>> groupedProducts)
        {
            Dictionary<bool, int> boolCount = groupedProducts.ToDictionary(g => g.Key, g => g.Count());
            Dictionary<int, int> intCount = new Dictionary<int, int>();
            intCount.Add(1, boolCount.ContainsKey(true) ? boolCount[true] : 0);
            intCount.Add(2, boolCount.ContainsKey(false) ? boolCount[false] : 0);
            return intCount;
        }


        //protected Dictionary<int, int> GetVisibilityMeta(IQueryable<Model> models, int[] stock, int[] maker, int[] stringLenght, decimal gaugeMin, decimal gaugeMax, int priceMin, int priceMax)
        //{
        //    // отфильтрованные продукты
        //    IQueryable<Product> products = FilterProduct(models, null, stock, priceMin, priceMax);

        //    var groupedProducts =
        //        from p in products
        //        group p by p.IsVisible into g
        //        select new
        //        {
        //            VisibleId = g.Key ? 1 : 2,
        //            Count = g.Count()
        //        };

        //    var visibilityCount =
        //        (from id in new int[] { 1, 2 }
        //         join g in groupedProducts
        //         on id equals g.VisibleId into res
        //         from lj in res.DefaultIfEmpty()
        //         select new
        //         {
        //             Id = id,
        //             Count = lj?.Count ?? 0
        //         }).ToDictionary(t => t.Id, t => t.Count);

        //    return visibilityCount;
        //}


        /// <summary>
        /// Фильтрует по общим параметрам и по параметрам конкретных типов, переданных аргументом
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="models"></param>
        /// <param name="visibility"></param>
        /// <param name="stock"></param>
        /// <param name="maker"></param>
        /// <param name="priceMin"></param>
        /// <param name="priceMax"></param>
        /// <param name="filters"></param>
        /// <returns></returns>
        protected IQueryable<TModel> FilterModels<TModel>(IQueryable<TModel> models, int[] visibility, int[] stock, int[] maker, int priceMin, int priceMax, List<Expression<Func<TModel, bool>>> filters) where TModel : Model
        {
            // по каким параметрам фильтровать модель
            if (maker != null && maker.Length != 0)
            {
                models = models.Where(m => maker.Contains(m.MakerId));
            }

            // фильтруем по параметрам конкретного типа
            foreach (var filter in filters)
            {
                models = models.Where(filter);
            }

            var productFilterCondition = ProductFilterCondition(visibility, stock, priceMin, priceMax);
            models = models.Where(m => m.Products.AsQueryable().Any(productFilterCondition));

            return models;
        }

        /// <summary>
        /// Фильтрует продукты
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="models"></param>
        /// <param name="visibility"></param>
        /// <param name="stock"></param>
        /// <param name="priceMin"></param>
        /// <param name="priceMax"></param>
        /// <returns></returns>
        protected IQueryable<Product> FilterProduct<TModel>(IQueryable<TModel> models, int[] visibility, int[] stock, int priceMin, int priceMax) where TModel : Model
        {
            // фильтруем продукты по моделям
            var filteredProductsByModel = _db.Products.Where(p => models.Select(m => m.Id).Contains(p.ModelId));

            // фильтруем продукты по видимости, наличию и цене
            var filteredProducts = filteredProductsByModel.Where(ProductFilterCondition(visibility, stock, priceMin, priceMax));

            return filteredProducts;
        }


        protected Expression<Func<Product, bool>> ProductFilterCondition(int[] visibility, int[] stock, int priceMin, int priceMax)
        {
            if (stock == null)
            {
                stock = new int[] { };
            }
            if (visibility == null)
            {
                visibility = new int[] { };
            }
            return p => (priceMin == 0 || p.ActualPrice > priceMin - 1) && (priceMax == 0 || p.ActualPrice < priceMax + 1) &&
            (visibility.Count() == 0 || (visibility.Contains(p.IsVisible ? 1 : 2))) &&
            (stock.Count() == 0 ||
                        (stock.Contains(1) && p.Count > _db.CheckOuts.Where(c => c.ProductId == p.Id).Select(c => c.Count).DefaultIfEmpty(0).Sum()) ||
                        (stock.Contains(2) && p.Count <= _db.CheckOuts.Where(c => c.ProductId == p.Id).Select(c => c.Count).DefaultIfEmpty(0).Sum() && p.IsOrder == true) ||
                        (stock.Contains(3) && p.Count <= _db.CheckOuts.Where(c => c.ProductId == p.Id).Select(c => c.Count).DefaultIfEmpty(0).Sum() && p.IsOrder == false));
        }

    }
}
