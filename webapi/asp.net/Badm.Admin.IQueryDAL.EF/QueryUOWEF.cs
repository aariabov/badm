﻿using Badm.Admin.IQueryDAL.Common;
using Badm.Admin.IQueryDAL.Others;
using Badm.Admin.IQueryDAL.Rackets;
using Badm.Admin.IQueryDAL.Strings;

namespace Badm.Admin.IQueryDAL.EF
{    
    public class QueryUOWEF : IQueryUOW
    {
        private readonly BadmContext _db;

        public QueryUOWEF(string connectionString)
        {
            
            _db = new BadmContext(connectionString);
            //_db.Configuration.AutoDetectChangesEnabled = false;
            //_db.Configuration.LazyLoadingEnabled = false;
            //_db.Configuration.ProxyCreationEnabled = false;
            //_db.Configuration.ValidateOnSaveEnabled = false;
        }

        // сокращенная запись свойства get{return...}
        public IStrings Strings => _stringsEf ?? (_stringsEf = new StringsEF(_db));
        private StringsEF _stringsEf;

        public IRackets Rackets => _racketsEf ?? (_racketsEf = new RacketsEF(_db));
        private RacketsEF _racketsEf;

        public IOthers Others => _othersEf ?? (_othersEf = new OthersEF(_db));
        private OthersEF _othersEf;

        public void Dispose()
        {
            _db?.Dispose();
        }

        
    }
}
