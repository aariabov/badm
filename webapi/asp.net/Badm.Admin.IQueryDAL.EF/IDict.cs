﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Badm.Admin.IQueryDAL.EF
{
    interface IDict
    {
        int Id { get; set; }
    }

    public partial class Maker : IDict{}
    public partial class StringLenght : IDict { }

    public partial class Balance : IDict { }
    public partial class Flex : IDict { }
    public partial class Level : IDict { }
    public partial class RacketMaterial : IDict { }
}
