﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Sockets;
using Badm.Admin.IQueryDAL.Common;
using Badm.Admin.IQueryDAL.Strings;

namespace Badm.Admin.IQueryDAL.EF
{
    class StringsEF : ModelsEF, IStrings
    {
        //private BadmContext _db;

        internal StringsEF(BadmContext db) : base(db)
        {
            //_db = db;
        }

        /// <summary>
        /// Информация о диапазонах цены и толщины струн (без фильтров)
        /// </summary>
        /// <returns></returns>
        public IEnumerable<FilterRange> GetFilterRanges()
        {
            List<FilterRange> filterRanges = new List<FilterRange>();

            MetaRange priceMetaRange = GetMetaRange(_db.Products.Where(p => _db.Models.OfType<String>().Select(m => m.Id).Contains(p.ModelId)), p => p.ActualPrice);
            MetaRange gaugeMetaRange = GetMetaRange(_db.Models.OfType<String>(), i => i.Gauge);

            filterRanges.Add(new FilterRange("price", priceMetaRange));
            filterRanges.Add(new FilterRange("gauge", gaugeMetaRange));

            return filterRanges;
        }

        public IEnumerable<StringDTO> Get(int[] visibility, int[] stock, int[] maker, int[] stringLenght, decimal gaugeMin, decimal gaugeMax, int priceMin, int priceMax, int page, int perPage, string sort)
        {
            // отфильтрованные струны
            IQueryable<String> models = FilterStrings(
                visibility: visibility, 
                stock: stock, 
                maker: maker, 
                stringLenght: stringLenght, 
                gaugeMin: gaugeMin, 
                gaugeMax: gaugeMax, 
                priceMin: priceMin, 
                priceMax: priceMax);

            // струны DTO
            IEnumerable<StringDTO> results = GetModels<String, StringDTO>(
                models: models, 
                visibility: visibility, 
                stock: stock, 
                maker: maker, 
                priceMin: priceMin, 
                priceMax: priceMax, 
                page: page, 
                perPage: perPage, 
                sort: sort);

            return results;
        }


        //public IEnumerable<StringDTO> Get(int[] visibility, int[] stock, int[] maker, int[] stringLenght, int page, int perPage, decimal gaugeMin = 0, decimal gaugeMax = 0, int priceMin = 0, int priceMax = 0, string sort = "date")
        //{            
        //    // отфильтрованные модели
        //    IQueryable<String> models = FilterStrings(visibility, stock, maker, stringLenght, gaugeMin, gaugeMax, priceMin, priceMax);

        //    // информация по количеству на чекауте
        //    var checkOutGroup = _db.CheckOuts.GroupBy(g => g.ProductId).Select(g => new { productId = g.Key, sum = g.Sum(c => c.Count) });

        //    var productFilterCondition = ProductFilterCondition(visibility, stock, priceMin, priceMax);

        //    // отфильтрованные модели + их отфильтрованные продукты + информация о наличии
        //    var results = models.Select(model => new
        //    {
        //        model,
        //        products =
        //            from p in model.Products.AsQueryable().Where(productFilterCondition)
        //            join c in checkOutGroup on p.Id equals c.productId into ps
        //            from lj in ps.DefaultIfEmpty()
        //            let checkOutCount = lj == null ? 0 : lj.sum
        //            select new ProductDTO
        //            {
        //                Id = p.Id,
        //                OldPrice = p.OldPrice,
        //                Date = p.Date,
        //                ActualPrice = p.ActualPrice,
        //                Count = p.Count ?? 0,
        //                ModelId = p.ModelId,
        //                CountCheckOut = checkOutCount,
        //                IsOrder = p.IsOrder,
        //                IsVisible = p.IsVisible,
        //                IsNew = p.IsNew,
        //                IsHit = p.IsHit,
        //                IsPromo = p.IsPromo,
        //                IsInteres = p.IsInteres
        //            }
        //    });

        //    string sortPropertyName = "";
        //    switch (sort)
        //    {
        //        case "date":
        //            results = results.OrderBy(m => m.products.Max(p => p.Date));
        //            sortPropertyName = nameof(Product.Date);
        //            break;
        //        case "-date":
        //            results = results.OrderByDescending(m => m.products.Max(p => p.Date));
        //            sortPropertyName = nameof(Product.Date);
        //            break;
        //        case "price":
        //            results = results.OrderBy(m => m.products.Max(p => p.ActualPrice));
        //            sortPropertyName = nameof(Product.ActualPrice);
        //            break;
        //        case "-price":
        //            results = results.OrderByDescending(m => m.products.Max(p => p.ActualPrice));
        //            sortPropertyName = nameof(Product.ActualPrice);
        //            break;
        //    }

        //    results = results.Skip((page - 1) * perPage).Take(perPage);

        //    var modelsWithProducts = results.AsEnumerable().Select(m => new StringDTO
        //    {
        //        Id = m.model.Id,
        //        Name = m.model.Name,
        //        CategoryId = m.model.CategoryId,
        //        MakerId = m.model.MakerId,
        //        Description = m.model.Description,
        //        StringLenghtId = m.model.StringLenghtId,
        //        Gauge = m.model.Gauge,

        //        Products = sort.StartsWith("-") ? m.products.AsEnumerable().OrderByDescending(p => p.GetType().GetProperty(sortPropertyName).GetValue(p, null)) :
        //                                            m.products.AsEnumerable().OrderBy(p => p.GetType().GetProperty(sortPropertyName).GetValue(p, null))
        //    }).Distinct().ToList();

        //    return modelsWithProducts;
        //}

        public StringsMeta GetMeta(int[] visibility, int[] stock, int[] maker, int[] stringLenght, decimal gaugeMin, decimal gaugeMax, int priceMin, int priceMax)
        {
            // отфильтрованные модели
            IQueryable<String> models = FilterStrings(
                visibility: visibility, 
                stock: stock, 
                maker: maker, 
                stringLenght: stringLenght, 
                gaugeMin: gaugeMin, 
                gaugeMax: gaugeMax, 
                priceMin: priceMin, 
                priceMax: priceMax);

            int totalCount = models.Count();

            MetaRange priceMetaRange = GetMetaRange(FilterProduct(models, visibility, stock, priceMin, priceMax), p => p.ActualPrice);
            MetaRange gaugeMetaRange = GetMetaRange(models, m => m.Gauge);

            Dictionary<int, int> visibilityMeta = GetMetaBool(GetStringProducts(
                                visibility: null, 
                                stock: stock, 
                                maker: maker, 
                                stringLenght: stringLenght, 
                                gaugeMin: gaugeMin, 
                                gaugeMax: gaugeMax, 
                                priceMin: priceMin, 
                                priceMax: priceMax).GroupBy(p => p.IsVisible));

            Dictionary<int, int> stockMeta = GetStockMeta(GetStringProducts(
                visibility: visibility,
                stock: null,
                maker: maker,
                stringLenght: stringLenght,
                gaugeMin: gaugeMin,
                gaugeMax: gaugeMax,
                priceMin: priceMin,
                priceMax: priceMax));


            Dictionary<int, int> makerMeta = GetMetaDictionary(_db.Makers, GetStringProducts(
                visibility: visibility, 
                stock: stock, 
                maker: null, // т.к. нужны количества по всем производителям, значит не надо фильтровать по выбранным
                stringLenght: stringLenght, 
                gaugeMin: gaugeMin, 
                gaugeMax: gaugeMax, 
                priceMin: priceMin, 
                priceMax: priceMax).GroupBy(p => p.Model.MakerId));
            
            Dictionary<int, int> stringLenghtMeta = GetMetaDictionary(_db.StringLenghts, GetStringProducts(
                visibility: visibility, 
                stock: stock, 
                maker: maker, 
                stringLenght: null, 
                gaugeMin: gaugeMin, 
                gaugeMax: gaugeMax, 
                priceMin: priceMin, 
                priceMax: priceMax).GroupBy(p => (p.Model as String).StringLenghtId));
            
            StringsMeta stringsMeta = new StringsMeta(
                visibility: visibilityMeta,
                stock: stockMeta,
                maker: makerMeta,
                price: priceMetaRange,
                totalCount: totalCount,
                stringLenght: stringLenghtMeta,
                gauge: gaugeMetaRange
                );

            return stringsMeta;
        }

        private IQueryable<Product> GetStringProducts(int[] visibility, int[] stock, int[] maker, int[] stringLenght, decimal gaugeMin, decimal gaugeMax, int priceMin, int priceMax)
        {
            var models = FilterStrings(
                visibility: visibility,
                stock: stock,
                maker: maker,
                stringLenght: stringLenght,
                gaugeMin: gaugeMin,
                gaugeMax: gaugeMax,
                priceMin: priceMin,
                priceMax: priceMax);

            var products = FilterProduct(models, visibility, stock, priceMin, priceMax);

            return products;
        }

        //private Dictionary<int, int> GetVisibilityMeta(int[] stock, int[] maker, int[] stringLenght, decimal gaugeMin, decimal gaugeMax, int priceMin, int priceMax)
        //{
        //    // отфильтрованные модели
        //    IQueryable<String> models = FilterStrings(null, stock, maker, stringLenght, gaugeMin, gaugeMax, priceMin, priceMax);

        //    // отфильтрованные продукты
        //    IQueryable<Product> products = FilterProduct(models, null, stock, priceMin, priceMax);

        //    var productVisibilityGroup =
        //        from p in products
        //        group p by p.IsVisible into g
        //        select new
        //        {
        //            VisibleId = g.Key ? 1 : 2,
        //            Count = g.Count()
        //        };

        //    var visibilityCount =
        //        (from id in new int[] { 1, 2 }
        //         join g in productVisibilityGroup on id equals g.VisibleId into res
        //         from lj in res.DefaultIfEmpty()
        //         select new
        //         {
        //             Id = id,
        //             Count = lj?.Count ?? 0
        //         }).ToDictionary(t => t.Id, t => t.Count);

        //    return visibilityCount;
        //}

        //private Dictionary<int, int> GetStockMeta(int[] visibility, int[] maker, int[] stringLenght, decimal gaugeMin, decimal gaugeMax, int priceMin, int priceMax)
        //{
        //    // отфильтрованные модели (не фильтруем по stock, т.к. нужны цифры по все значениям)
        //    IQueryable<String> models = FilterStrings(visibility, null, maker, stringLenght, gaugeMin, gaugeMax, priceMin, priceMax);

        //    // отфильтрованные продукты (не фильтруем по stock, т.к. нужны цифры по все значениям)
        //    IQueryable<Product> products = FilterProduct(models, visibility, null, priceMin, priceMax);

        //    // информация по количеству на чекауте
        //    var checkOutGroup = _db.CheckOuts.GroupBy(g => g.ProductId).Select(g => new { productId = g.Key, sum = g.Sum(c => c.Count) });

        //    // информация о наличии продукта: 1 - в наличии, 2 - под заказ, 3 - нет в наличии
        //    var productStock =
        //        from p in products
        //        join c in checkOutGroup on p.Id equals c.productId into pc
        //        from lj in pc.DefaultIfEmpty()
        //        let checkOutCount = lj == null ? 0 : lj.sum
        //        select new
        //        {
        //            StockId = p.Count > checkOutCount ? 1 : (p.IsOrder == true ? 2 : 3)
        //        };

        //    // группировка по ключам stock - показывает кол-во продуктов в наличии, под заказ и нет в наличии (значения могут отсутствовать)
        //    var productStockGroup = 
        //        from p in productStock
        //        group p by p.StockId into g
        //        select new
        //        {
        //            StockId = g.Key,
        //            Count = g.Count()
        //        };

        //    // left join ключей stock и productStockGroup, чтобы запонить отсутствующие значения
        //    var stockCount = 
        //        (from id in new int[] { 1, 2, 3}
        //        join g in productStockGroup on id equals g.StockId into res
        //        from lj in res.DefaultIfEmpty()
        //        select new
        //        {
        //            Id = id,
        //            Count = lj?.Count ?? 0
        //        }).ToDictionary(t => t.Id, t => t.Count);

        //    return stockCount;
        //}

        

        /// <summary>
        /// Количество продуктов на каждое значение производителя
        /// </summary>
        /// <param name="products"></param>
        /// <returns></returns>
        //private Dictionary<int, int> GetMakerMeta(int[] visibility, int[] stock, int[] stringLenght, decimal gaugeMin, decimal gaugeMax, int priceMin, int priceMax)
        //{
        //    // отфильтрованные модели (не фильтруем по maker, т.к. нужны цифры по все значениям)
        //    IQueryable<String> models = FilterStrings(visibility, stock, null, stringLenght, gaugeMin, gaugeMax, priceMin, priceMax);

        //    // отфильтрованные продукты
        //    IQueryable<Product> products = FilterProduct(models, visibility, stock, priceMin, priceMax);

        //    // нужны только продукты, информацию о производители можно получить из свойства
        //    // делаем left join всех производителей с производителями продуктов, и получаем по каждому производителю кол-во продуктов
        //    var makersCount =
        //        (from maker in _db.Makers
        //         join g in (from product in products
        //                    group product by product.Model.MakerId)
        //         on maker.Id equals g.Key into outer
        //         from res in outer.DefaultIfEmpty()
        //         select new
        //         {
        //             Id = maker.Id,
        //             Count = res.Count()
        //         }).ToDictionary(t => t.Id, t => t.Count);

        //    return makersCount;
        //}

        /// <summary>
        /// Количество продуктов на каждое значение длины струны
        /// </summary>
        /// <param name="products"></param>
        /// <returns></returns>
        //private Dictionary<int, int> GetStringLenghtMeta(int[] visibility, int[] stock, int[] maker, decimal gaugeMin, decimal gaugeMax, int priceMin, int priceMax)
        //{
        //    // отфильтрованные модели (не фильтруем по stringLenght, т.к. нужны цифры по все значениям)
        //    IQueryable<String> models = FilterStrings(visibility, stock, maker, null, gaugeMin, gaugeMax, priceMin, priceMax);

        //    // отфильтрованные продукты
        //    IQueryable<Product> products = FilterProduct(models, visibility, stock, priceMin, priceMax);

        //    // нужны только продукты, информацию о длине струны можно получить из свойства
        //    // делаем left join всех длин струн с длинами струн продуктов, и получаем по каждой длине струны кол-во продуктов
        //    var stringLenghtCount =
        //        (from stringLenght in _db.StringLenghts
        //         join g in (from product in products
        //                    group product by (product.Model as String).StringLenghtId)
        //          on stringLenght.Id equals g.Key into outer
        //         from res in outer.DefaultIfEmpty()
        //         select new
        //         {
        //             Id = stringLenght.Id,
        //             Count = res.Count()
        //         }).ToDictionary(t => t.Id, t => t.Count);

        //    return stringLenghtCount;
        //}

        private Dictionary<int, int> GetStringLenghtMeta(int[] visibility, int[] stock, int[] maker, decimal gaugeMin, decimal gaugeMax, int priceMin, int priceMax)
        {
            // отфильтрованные модели (не фильтруем по stringLenght, т.к. нужны цифры по все значениям)
            IQueryable<String> models = FilterStrings(visibility, stock, maker, null, gaugeMin, gaugeMax, priceMin, priceMax);

            // отфильтрованные продукты
            IQueryable<Product> products = FilterProduct(models, visibility, stock, priceMin, priceMax);

            // нужны только продукты, информацию о длине струны можно получить из свойства
            // делаем left join всех длин струн с длинами струн продуктов, и получаем по каждой длине струны кол-во продуктов
            var stringLenghtCount =
                (from stringLenght in _db.StringLenghts
                 join g in products.GroupBy(p=>(p.Model as String).StringLenghtId)
                  on stringLenght.Id equals g.Key into outer
                 from res in outer.DefaultIfEmpty()
                 select new
                 {
                     Id = stringLenght.Id,
                     Count = res.Count()
                 }).ToDictionary(t => t.Id, t => t.Count);

            return stringLenghtCount;
        }


        //private MetaRange GetPriceRange(IQueryable<Product> products)
        //{            
        //    // нужны только продукты
        //    var priceRangeResult = products
        //        .GroupBy(i => 1)
        //        .Select(g => new
        //        {                    
        //            priceMax = g.Max(i => i.ActualPrice),
        //            priceMin = g.Min(i => i.ActualPrice)
        //        }).ToList();
        //    // защита на случай, если не оказалось продуктов
        //    if (priceRangeResult.Count == 0)
        //    {
        //        return null;
        //    }                
        //    else
        //    {
        //        var priceRange = priceRangeResult.First();
        //        return new MetaRange(priceRange.priceMin, priceRange.priceMax);
        //    }
        //}

        private MetaRange GetGaugeRange(IQueryable<String> models)
        {
            // нужны только модели
            var modelRangesResult = models
                .GroupBy(i => 1)
                .Select(g => new
                {
                    gaugeMax = g.Max(i => i.Gauge),
                    gaugeMin = g.Min(i => i.Gauge)
                }).ToList();
            // защита на случай, если не оказалось моделей
            if (modelRangesResult.Count == 0)
            {
                return null;
            }
            else
            {
                var modelRanges = modelRangesResult.ToList().First();
                return new MetaRange(modelRanges.gaugeMin, modelRanges.gaugeMax);
            }            
        }

        /// <summary>
        /// Фильтрует продукты моделей по видимости, наличию и цене
        /// </summary>
        /// <param name="models"></param>
        /// <param name="stock"></param>
        /// <param name="priceMin"></param>
        /// <param name="priceMax"></param>
        /// <returns></returns>
        //private IQueryable<Product> FilterProduct(IQueryable<String> models, int[] visibility, int[] stock, int priceMin, int priceMax)
        //{
        //    // фильтруем продукты по моделям
        //    var filteredProductsByModel = _db.Products.Where(p => models.Select(m => m.Id).Contains(p.ModelId));

        //    // фильтруем продукты по видимости, наличию и цене
        //    var filteredProductsByStockAndPrice = filteredProductsByModel.Where(ProductFilterCondition(visibility, stock, priceMin, priceMax));

        //    return filteredProductsByStockAndPrice;
        //}

        private IQueryable<String> FilterStrings(int[] visibility, int[] stock, int[] maker, int[] stringLenght, decimal gaugeMin, decimal gaugeMax, int priceMin, int priceMax)
        {
            List<Expression<Func<String, bool>>> filters = new List<Expression<Func<String, bool>>>();

            // базовый запрос
            var models = _db.Models.OfType<String>();
            
            if (stringLenght != null && stringLenght.Length != 0)
            {
                filters.Add(m => stringLenght.Contains(m.StringLenghtId));
            }
            if (gaugeMin != 0)
            {
                filters.Add(m => m.Gauge > gaugeMin - 0.01m);
            }
            if (gaugeMax != 0)
            {
                filters.Add(m => m.Gauge < gaugeMax + 0.01m);
            }

            // фильтруем по общим параметрам + по параметрам конкретного типа (filters)
            models = FilterModels(
                models: models, 
                visibility: visibility, 
                stock: stock, 
                maker: maker, 
                priceMin: priceMin, 
                priceMax: priceMax,
                filters: filters);

            return models;
        }

        /// <summary>
        /// Фильтрует модель по параметрам, по цене продуктов и по наличию
        /// </summary>
        /// <param name="maker"></param>
        /// <param name="stringLenght"></param>
        /// <param name="gaugeMin"></param>
        /// <param name="gaugeMax"></param>
        /// <param name="priceMin"></param>
        /// <param name="priceMax"></param>
        /// <returns></returns>
        //private IQueryable<String> FilterStrings(int[] visibility, int[] stock, int[] maker, int[] stringLenght, decimal gaugeMin = 0, decimal gaugeMax = 0, int priceMin = 0, int priceMax = 0)
        //{            
        //    // базовый запрос
        //    var models = _db.Models.OfType<String>();

        //    // по каким параметрам фильтровать модель
        //    if (maker != null && maker.Length != 0)
        //    {
        //        models = models.Where(m => maker.Contains(m.MakerId));
        //    }
        //    if (stringLenght != null && stringLenght.Length != 0)
        //    {
        //        models = models.Where(m => stringLenght.Contains(m.StringLenghtId));
        //    }
        //    if (gaugeMin != 0)
        //    {
        //        models = models.Where(m => m.Gauge > gaugeMin - 0.01m);
        //    }
        //    if (gaugeMax != 0)
        //    {
        //        models = models.Where(m => m.Gauge < gaugeMax + 0.01m);
        //    }

        //    var productFilterCondition = ProductFilterCondition(visibility, stock, priceMin, priceMax);
        //    models = models.Where(m => m.Products.AsQueryable().Any(productFilterCondition));

        //    return models;
        //}


        /// <summary>
        /// Условие для фильтрации продуктов по наличию и цене
        /// </summary>
        /// <param name="stock"></param>
        /// <param name="priceMin"></param>
        /// <param name="priceMax"></param>
        /// <returns></returns>
        //private Expression<Func<Product, bool>> ProductFilterCondition(int[] visibility, int[] stock, int priceMin = 0, int priceMax = 0)
        //{
        //    if (stock == null)
        //    {
        //        stock = new int[]{};
        //    }
        //    if (visibility == null)
        //    {
        //        visibility = new int[] { };
        //    }
        //    return p => (priceMin == 0 || p.ActualPrice > priceMin - 1) && (priceMax == 0 || p.ActualPrice < priceMax + 1) &&
        //    (visibility.Count() == 0 || (visibility.Contains(p.IsVisible ? 1 : 2))) &&
        //    (stock.Count() == 0 ||
        //                (stock.Contains(1) && p.Count > _db.CheckOuts.Where(c => c.ProductId == p.Id).Select(c => c.Count).DefaultIfEmpty(0).Sum()) ||
        //                (stock.Contains(2) && p.Count <= _db.CheckOuts.Where(c => c.ProductId == p.Id).Select(c => c.Count).DefaultIfEmpty(0).Sum() && p.IsOrder == true) ||
        //                (stock.Contains(3) && p.Count <= _db.CheckOuts.Where(c => c.ProductId == p.Id).Select(c => c.Count).DefaultIfEmpty(0).Sum() && p.IsOrder == false));
        //}

    }
}
