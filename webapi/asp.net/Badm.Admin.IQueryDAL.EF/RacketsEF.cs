﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Badm.Admin.IQueryDAL.Common;
using Badm.Admin.IQueryDAL.Rackets;
using Badm.Admin.IQueryDAL.Strings;

namespace Badm.Admin.IQueryDAL.EF
{
    class RacketsEF : ModelsEF, IRackets
    {
        public RacketsEF(BadmContext db) : base(db){}

        public IEnumerable<FilterRange> GetFilterRanges()
        {
            List<FilterRange> filterRanges = new List<FilterRange>();
            MetaRange priceMetaRange = GetMetaRange(_db.Products.Where(p => _db.Models.OfType<Racket>().Select(m => m.Id).Contains(p.ModelId)), p => p.ActualPrice);
            MetaRange weightMetaRange = GetMetaRange(_db.Models.OfType<Racket>(), i => i.Weight);
            MetaRange lenghtMetaRange = GetMetaRange(_db.Models.OfType<Racket>(), i => i.Lenght);

            filterRanges.Add(new FilterRange("price", priceMetaRange));
            filterRanges.Add(new FilterRange("weight", weightMetaRange));
            filterRanges.Add(new FilterRange("lenght", lenghtMetaRange));

            return filterRanges;
        }

        public IEnumerable<RacketDTO> Get(int[] visibility, int[] stock, int[] maker, int[] balance, int[] flex, int[] headMaterial, int[] shaftMaterial, int[] level, int weightMin, int weightMax, int lenghtMin, int lenghtMax, int priceMin, int priceMax, int page, int perPage, string sort)
        {
            // отфильтрованные модели
            IQueryable<Racket> models = FilterRackets(
                visibility: visibility, 
                stock: stock, 
                maker: maker, 
                balance: balance, 
                flex: flex, 
                headMaterial: headMaterial, 
                shaftMaterial: shaftMaterial, 
                level: level, 
                weightMin: weightMin, 
                weightMax: weightMax, 
                lenghtMin: lenghtMin, 
                lenghtMax: lenghtMax, 
                priceMin: priceMin, 
                priceMax: priceMax);

            var results = GetModels<Racket, RacketDTO>(
                models: models,
                visibility: visibility,
                stock: stock,
                maker: maker,
                priceMin: priceMin,
                priceMax: priceMax,
                page: page,
                perPage: perPage,
                sort: sort);

            return results;
        }

        public RacketsMeta GetMeta(int[] visibility, int[] stock, int[] maker, int[] balance, int[] flex, int[] headMaterial, int[] shaftMaterial, int[] level, int weightMin, int weightMax, int lenghtMin, int lenghtMax, int priceMin, int priceMax)
        {
            // отфильтрованные модели
            IQueryable<Racket> models = FilterRackets(
                visibility: visibility, 
                stock: stock, 
                maker: maker, 
                balance: balance, 
                flex: flex, 
                headMaterial: headMaterial, 
                shaftMaterial: shaftMaterial, 
                level: level, 
                weightMin: weightMin, 
                weightMax: weightMax, 
                lenghtMin: lenghtMin, 
                lenghtMax: lenghtMax, 
                priceMin: priceMin, 
                priceMax: priceMax);

            int totalCount = models.Count();

            MetaRange priceMetaRange = GetMetaRange(FilterProduct(models, visibility, stock, priceMin, priceMax), p => p.ActualPrice);
            MetaRange lenghtMetaRange = GetMetaRange(models, m => m.Lenght);
            MetaRange weightMetaRange = GetMetaRange(models, m => m.Weight);

            Dictionary<int, int> visibilityMeta = GetMetaBool(GetRacketProducts(
                visibility: null,
                stock: stock,
                maker: maker,
                balance: balance,
                flex: flex,
                headMaterial: headMaterial,
                shaftMaterial: shaftMaterial,
                level: level,
                weightMin: weightMin,
                weightMax: weightMax,
                lenghtMin: lenghtMin,
                lenghtMax: lenghtMax,
                priceMin: priceMin,
                priceMax: priceMax).GroupBy(p => p.IsVisible));

            Dictionary<int, int> stockMeta = GetStockMeta(GetRacketProducts(
                visibility: visibility,
                stock: null,
                maker: maker,
                balance: balance,
                flex: flex,
                headMaterial: headMaterial,
                shaftMaterial: shaftMaterial,
                level: level,
                weightMin: weightMin,
                weightMax: weightMax,
                lenghtMin: lenghtMin,
                lenghtMax: lenghtMax,
                priceMin: priceMin,
                priceMax: priceMax));


            Dictionary<int, int> makerMeta = GetMetaDictionary(_db.Makers, GetRacketProducts(
                visibility: visibility,
                stock: stock,
                maker: null,
                balance: balance,
                flex: flex,
                headMaterial: headMaterial,
                shaftMaterial: shaftMaterial,
                level: level,
                weightMin: weightMin,
                weightMax: weightMax,
                lenghtMin: lenghtMin,
                lenghtMax: lenghtMax,
                priceMin: priceMin,
                priceMax: priceMax).GroupBy(p => p.Model.MakerId));

            Dictionary<int, int> balanceMeta = GetMetaDictionary(_db.Balances, GetRacketProducts(
                visibility: visibility,
                stock: stock,
                maker: maker,
                balance: null,
                flex: flex,
                headMaterial: headMaterial,
                shaftMaterial: shaftMaterial,
                level: level,
                weightMin: weightMin,
                weightMax: weightMax,
                lenghtMin: lenghtMin,
                lenghtMax: lenghtMax,
                priceMin: priceMin,
                priceMax: priceMax).GroupBy(p => (p.Model as Racket).BalanceId));

            Dictionary<int, int> flexMeta = GetMetaDictionary(_db.Flexes, GetRacketProducts(
                visibility: visibility,
                stock: stock,
                maker: maker,
                balance: balance,
                flex: null,
                headMaterial: headMaterial,
                shaftMaterial: shaftMaterial,
                level: level,
                weightMin: weightMin,
                weightMax: weightMax,
                lenghtMin: lenghtMin,
                lenghtMax: lenghtMax,
                priceMin: priceMin,
                priceMax: priceMax).GroupBy(p => (p.Model as Racket).FlexId));

            Dictionary<int, int> levelMeta = GetMetaDictionary(_db.Levels, GetRacketProducts(
                visibility: visibility,
                stock: stock,
                maker: maker,
                balance: balance,
                flex: flex,
                headMaterial: headMaterial,
                shaftMaterial: shaftMaterial,
                level: null,
                weightMin: weightMin,
                weightMax: weightMax,
                lenghtMin: lenghtMin,
                lenghtMax: lenghtMax,
                priceMin: priceMin,
                priceMax: priceMax).GroupBy(p => (p.Model as Racket).LevelId));

            Dictionary<int, int> headMaterialMeta = GetMetaDictionary(_db.RacketMaterials, GetRacketProducts(
                visibility: visibility,
                stock: stock,
                maker: maker,
                balance: balance,
                flex: flex,
                headMaterial: null,
                shaftMaterial: shaftMaterial,
                level: level,
                weightMin: weightMin,
                weightMax: weightMax,
                lenghtMin: lenghtMin,
                lenghtMax: lenghtMax,
                priceMin: priceMin,
                priceMax: priceMax).GroupBy(p => (p.Model as Racket).HeadMaterialId));

            Dictionary<int, int> shaftMaterialMeta = GetMetaDictionary(_db.RacketMaterials, GetRacketProducts(
                visibility: visibility,
                stock: stock,
                maker: maker,
                balance: balance,
                flex: flex,
                headMaterial: headMaterial,
                shaftMaterial: null,
                level: level,
                weightMin: weightMin,
                weightMax: weightMax,
                lenghtMin: lenghtMin,
                lenghtMax: lenghtMax,
                priceMin: priceMin,
                priceMax: priceMax).GroupBy(p => (p.Model as Racket).ShaftMaterialId));

            RacketsMeta racketsMeta = new RacketsMeta(
                visibility: visibilityMeta, 
                stock: stockMeta, 
                maker: makerMeta, 
                price: priceMetaRange, 
                totalCount: totalCount, 
                level: levelMeta, 
                balance: balanceMeta, 
                flex: flexMeta, 
                shaftMaterial: shaftMaterialMeta, 
                headMaterial: headMaterialMeta, 
                weight: weightMetaRange, 
                lenght: lenghtMetaRange);

            return racketsMeta;
        }

        private IQueryable<Product> GetRacketProducts(int[] visibility, int[] stock, int[] maker, int[] balance, int[] flex, int[] headMaterial, int[] shaftMaterial, int[] level, int weightMin, int weightMax, int lenghtMin, int lenghtMax, int priceMin, int priceMax)
        {
            var models = FilterRackets(
                visibility: visibility,
                stock: stock,
                maker: maker,
                balance: balance,
                flex: flex,
                headMaterial: headMaterial,
                shaftMaterial: shaftMaterial,
                level: level,
                weightMin: weightMin,
                weightMax: weightMax,
                lenghtMin: lenghtMin,
                lenghtMax: lenghtMax,
                priceMin: priceMin,
                priceMax: priceMax);

            var products = FilterProduct(models, visibility, stock, priceMin, priceMax);

            return products;
        }


        private IQueryable<Racket> FilterRackets(int[] visibility, int[] stock, int[] maker, int[] balance, int[] flex, int[] headMaterial, int[] shaftMaterial, int[] level, int weightMin, int weightMax, int lenghtMin, int lenghtMax, int priceMin, int priceMax)
        {
            List<Expression<Func<Racket, bool>>> filters = new List<Expression<Func<Racket, bool>>>();

            // базовый запрос
            var models = _db.Models.OfType<Racket>();

            if (balance != null && balance.Length != 0)
            {
                filters.Add(m => balance.Contains(m.BalanceId));
            }
            if (flex != null && flex.Length != 0)
            {
                filters.Add(m => flex.Contains(m.FlexId));
            }
            if (headMaterial != null && headMaterial.Length != 0)
            {
                filters.Add(m => headMaterial.Contains(m.HeadMaterialId));
            }
            if (shaftMaterial != null && shaftMaterial.Length != 0)
            {
                filters.Add(m => shaftMaterial.Contains(m.ShaftMaterialId));
            }
            if (level != null && level.Length != 0)
            {
                filters.Add(m => level.Contains(m.LevelId));
            }
            if (weightMin != 0)
            {
                filters.Add(m => m.Weight > weightMin - 1);
            }
            if (weightMax != 0)
            {
                filters.Add(m => m.Weight < weightMax + 1);
            }
            if (lenghtMin != 0)
            {
                filters.Add(m => m.Lenght > lenghtMin - 1);
            }
            if (lenghtMax != 0)
            {
                filters.Add(m => m.Lenght < lenghtMax + 1);
            }

            models = FilterModels(
                models: models, 
                visibility: visibility, 
                stock: stock, 
                maker: maker, 
                priceMin: priceMin, 
                priceMax: priceMax, 
                filters: filters);

            return models;
        }

        //MyClass metaRange = _db.Database.SqlQuery<MyClass>("SELECT MIN(Lenght) as 'min', MAX(Lenght) as 'max' FROM Racket").First();
        //public class MyClass
        //{
        //    public int min { get; set; }
        //    public int max { get; set; }
        //}

    }
}
