﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Badm.Admin.IQueryDAL.Common;
using Badm.Admin.IQueryDAL.Others;
using Badm.Admin.IQueryDAL.Strings;

namespace Badm.Admin.IQueryDAL.EF
{
    class OthersEF : ModelsEF, IOthers
    {
        public OthersEF(BadmContext db) : base(db)
        {
        }

        public IEnumerable<FilterRange> GetFilterRanges()
        {
            List<FilterRange> filterRanges = new List<FilterRange>();

            MetaRange priceMetaRange = GetMetaRange(_db.Products.Where(p => _db.Models.OfType<String>().Select(m => m.Id).Contains(p.ModelId)), p => p.ActualPrice);

            filterRanges.Add(new FilterRange("price", priceMetaRange));

            return filterRanges;
        }

        public IEnumerable<OtherDTO> Get(int[] visibility, int[] stock, int[] maker, int page, int perPage, int priceMin = 0, int priceMax = 0,
            string sort = "date")
        {
            throw new NotImplementedException();
        }

        public StringsMeta GetMeta(int[] visibility, int[] stock, int[] maker, int priceMin = 0, int priceMax = 0)
        {
            throw new NotImplementedException();
        }
    }
}
