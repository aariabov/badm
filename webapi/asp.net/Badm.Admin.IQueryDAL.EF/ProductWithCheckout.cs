﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Badm.Admin.IQueryDAL.EF
{
    public class ProductWithCheckout
    {
        public Product Product;
        public Check Check;
    }

    public class ModelWithProducts<TModel>
    {
        public IQueryable<Product> products;
        public TModel model;
    }

    public class Check
    {
        public int ProductId;
        public int CountCheckOut;
    }
}
