﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Badm.Admin.IQueryDAL.EF
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class BadmContext : DbContext
    {
        public BadmContext(string connectionString)
            : base(connectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<BagFront> BagFronts { get; set; }
        public virtual DbSet<BagType> BagTypes { get; set; }
        public virtual DbSet<Balance> Balances { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<CheckOut> CheckOuts { get; set; }
        public virtual DbSet<CheckOut_ShoesSize> CheckOut_ShoesSize { get; set; }
        public virtual DbSet<CheckOut_WearSize> CheckOut_WearSize { get; set; }
        public virtual DbSet<Color> Colors { get; set; }
        public virtual DbSet<Cover> Covers { get; set; }
        public virtual DbSet<Flex> Flexes { get; set; }
        public virtual DbSet<GripMaterial> GripMaterials { get; set; }
        public virtual DbSet<GripType> GripTypes { get; set; }
        public virtual DbSet<Level> Levels { get; set; }
        public virtual DbSet<Maker> Makers { get; set; }
        public virtual DbSet<Model> Models { get; set; }
        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<PostType> PostTypes { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<RacketMaterial> RacketMaterials { get; set; }
        public virtual DbSet<Search> Searches { get; set; }
        public virtual DbSet<Sex> Sexes { get; set; }
        public virtual DbSet<Shoes_ShoesSize> Shoes_ShoesSize { get; set; }
        public virtual DbSet<ShoesSize> ShoesSizes { get; set; }
        public virtual DbSet<Strap> Straps { get; set; }
        public virtual DbSet<StringLenght> StringLenghts { get; set; }
        public virtual DbSet<Trade> Trades { get; set; }
        public virtual DbSet<VolanCount> VolanCounts { get; set; }
        public virtual DbSet<VolanSpeed> VolanSpeeds { get; set; }
        public virtual DbSet<VolanType> VolanTypes { get; set; }
        public virtual DbSet<Wear_WearMaterial> Wear_WearMaterial { get; set; }
        public virtual DbSet<Wear_WearSize> Wear_WearSize { get; set; }
        public virtual DbSet<WearMaterial> WearMaterials { get; set; }
        public virtual DbSet<WearSize> WearSizes { get; set; }
        public virtual DbSet<WearType> WearTypes { get; set; }
        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
    }
}
