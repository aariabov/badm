﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Badm.Admin.IQueryDAL.Common;

namespace Badm.Admin.IQueryDAL.EF
{
    public class ModelProducts<TModel> where TModel : Model
    {
        public TModel model;
        public IQueryable<ProductDTO> products;
    }
}
