﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Badm.IIdentity
{
    public class AppUser : IdentityUser // наследуемся от IdentityUser, который реализован в Microsoft.AspNet.Identity.EntityFramework, потому что UserStore (реализация IUserStore от Microsoft) требует юзера этого типа
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(IUserManager manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }

    }
}
