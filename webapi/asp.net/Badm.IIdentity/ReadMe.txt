﻿Интерфейс для подсистемы идентификации
Сущности:
AppUser - унаследован от IdentityUser, содержит стандартные поля, при необходимости можно добавлять поля
IUserManager - интерфейс доступа к менеджеру пользователей. Можно использовать стандартный майкросовтовский менеджер (свой писать долго и сложно). 
Чтобы сменить хранилище, надо реализовать интерфейс IUserStore (он наследуется от других интерфейсов, необязательно реализовывать их все - надо только нужные)

Ссылки:
https://docs.microsoft.com/en-us/aspnet/identity/overview/extensibility/overview-of-custom-storage-providers-for-aspnet-identity#architecture
https://docs.microsoft.com/en-us/aspnet/identity/overview/extensibility/implementing-a-custom-mysql-aspnet-identity-storage-provider
http://www.pzone.ru/web/asp-net/mvc5-identity-without-ef/