﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.OAuth;

namespace Badm.IIdentity
{
    /// <summary>
    /// Интерфейс доступа к UserManager (можем ограничить стандартную функциональность AppUserManager, реализованного Microsoft)
    /// </summary>
    public interface IUserManager : IDisposable 
    {
        Task<IdentityResult> CreateAsync(AppUser user, string password);
        Task<IdentityResult> CreateAsync(AppUser user);
        Task<IdentityResult> ConfirmEmailAsync(string userId, string token);
        Task<AppUser> FindByNameAsync(string userName);
        Task<AppUser> FindAsync(string userName, string password);
        Task<ClaimsIdentity> CreateIdentityAsync(AppUser user, string authenticationType);
        Task<bool> IsEmailConfirmedAsync(string userId);
        Task<IdentityResult> ResetPasswordAsync(string userId, string token, string newPassword);
        Task<IList<string>> GetValidTwoFactorProvidersAsync(string userId);
        Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login);
        void Dispose(bool disposing);
        void Dispose();
    }
}
