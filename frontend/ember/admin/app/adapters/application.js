﻿import DS from 'ember-data';
import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';

export default DS.JSONAPIAdapter.extend(DataAdapterMixin, {
    namespace: 'api',
    authorizer: 'authorizer:application',

    shouldBackgroundReloadAll(store, snapshotArray) {
        return false;
    },

    shouldBackgroundReloadRecord(store, snapshotArray) {
        return false;
    }

});