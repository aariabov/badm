﻿import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function () {
  this.route('index', { path: '/' }, function() {});
  this.route('login');
  this.route('strings');
  this.route('rackets');
  this.route('other');
  this.route('test');
});

export default Router;
