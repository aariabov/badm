﻿import Ember from 'ember';

export default Ember.TextField.extend({
    tagName: "input",
    type: "text",
    //attributeBindings: ["type", "value"],
    keyDown: function (e) {
        var key = e.charCode || e.keyCode || 0;
        // allow enter, backspace, tab, delete, numbers, keypad numbers, home, end only.
        var isNumberValidKeys = (
            key === 13 ||
            key === 8 ||
            key === 9 ||
            key === 46 ||
            (key >= 35 && key <= 40) ||
            (key >= 48 && key <= 57) ||
            (key >= 96 && key <= 105));

        // русская и английская точки и вводят первый раз        
        var isDotValidKeys = (this.value.toString().indexOf('.') === -1 && e.key === '.');

        var isValid = false;
        if (this.digits) {
            // если double, то можно вводить точку
            isValid = isNumberValidKeys || isDotValidKeys;
        } else {
            isValid = isNumberValidKeys;
        }
       
        return isValid;
    },

    focusIn: function () {
        this.set('oldVal', this.value);
        console.log('focusIn ' + this.get('oldVal'));
        console.log('min ' + this.min);
        console.log('max ' + this.max);
    },
    //keyPress: function () {
    //    console.log(this.value);
    //    var inputValue = this.value || "";
    //    return (inputValue.length < 3000);
    //},
    change: function () {
        if (this.value >= this.min && this.value <= this.max) {
            if (this.digits) {
                this.set('value', Math.round(this.value * 100) / 100);
            }
            console.log('change ' + this.value);
            this.set('oldVal', this.value);
            this.get('on-change')(this.value);
        } else {            
            this.set('value', this.oldVal);
            console.log('old ' + this.oldVal);
            console.log('value ' + this.value);
            console.log('getvalue ' + this.get('value'));
        }
    }
});