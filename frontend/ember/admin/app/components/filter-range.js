﻿import Ember from 'ember';

export default Ember.Component.extend({
    // значение min input
    startMin: Ember.computed('start', function () {
        console.log('computed ');
        return this.get('start')[0];
    }),
    // значение max input
    startMax: Ember.computed('start', function () {
        return this.get('start')[1];
    }),

    digits: Ember.computed('step', function () {
        let step = this.get('step').toString();
        let s = step.match(/\.\d+$/);
        if (s) {
            return s[0].length - 1;
        }        
        return null;
    }),

    updateStartSlider() {
        this.set("start", [this.get('startMin'), this.get('startMax')]);
        this.get('on-change')(this.get('start'), this.get('name'));
    },

    actions: {

        // обновляет startMin слайдера, которое задали в inputе
        updateStartMinSlider(val) {
            this.set('startMin', val);
            this.updateStartSlider();
        },

        // обновляет startMin слайдера, которое задали в inputе
        updateStartMaxSlider(val) {
            this.set('startMax', val);
            this.updateStartSlider();
        },

        // обновляет значения в input-ах при изменении значений в слайдере
        updateInputs(val) {
            if (this.get('startMin') !== val[0]) {
                this.set('startMin', val[0]);
            }
            if (this.get('startMax') !== val[1]) {
                this.set('startMax', val[1]);
            }            
        }

    }

});
