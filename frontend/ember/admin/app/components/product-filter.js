﻿import Ember from 'ember';

export default Ember.Component.extend({
    sort: 'name',
    perPage: 1,
    page: 1,

    init() {
        this._super(...arguments);
        this.get('findAll')(this.get('sort')).then((results) => this.set('results', results));
    },

    actions: {
        sortBy(prop) {
            switch (prop) {
                case 'id':
                    this.set('sort', this.get('sort') === 'id' ? '-id' : 'id');
                    break;
                case 'name':
                    this.set('sort', this.get('sort') === 'name' ? '-name' : 'name');
                    break;
            }
            //this.get('findAll')(this.get('sort')).then((results) => this.set('results', results));
        },
    }
});
