﻿import RangeSlider from 'ember-cli-nouislider/components/range-slider';

export default RangeSlider.extend({
    //start: [this.get('startMin'), this.get('startMax')], // 4 handles, starting at...
    connect: true, // Display a colored bar between the handles
    //behaviour: 'tap-drag', // Move handle on tap, bar is draggable
    step: 1,
    tooltips: true,   
    format: {
        to: function (value) {
            return Math.round(value * 100) / 100;
        },
        from: function (value) {
            return Math.round(value * 100) / 100;
        }
    },
    

    init() {
        //console.log('init RangeSlider '+this.name);
        this._super(...arguments);
        //console.log(this.startMin);
        //console.log(this.startMax);
        //if (!this.start) {
        //    this.start = [this.rangeMin, this.rangeMax];
        //}
        
        //this.range = {
        //    min: this.rangeMin,
        //    max: this.rangeMax
        //}
    }
});