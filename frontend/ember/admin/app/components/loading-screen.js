﻿import Ember from 'ember';

export default Ember.Component.extend({
    loading: Ember.inject.service('loading'),
    classNames: 'loading'
});
