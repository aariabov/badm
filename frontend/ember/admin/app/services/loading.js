﻿import Ember from 'ember';

export default Ember.Service.extend({
    isShow: false,

    show() {
        this.set("isShow", true);
    },
    hide() {
        this.set("isShow", false);
    }
});
