﻿export function initialize(application) {
    //application.inject('route', 'loading', 'service:loading');
    // внедряем сервис во все контроллеры
    application.inject('controller', 'loading', 'service:loading');
}

export default {
  name: 'loading',
  initialize: initialize
};