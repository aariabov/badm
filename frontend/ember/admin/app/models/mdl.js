﻿import Ember from 'ember';
import DS from 'ember-data';
import Dicts from '../enums/dictionaries';

export default DS.Model.extend({
    name: DS.attr(),
    description: DS.attr(),
    makerId: DS.attr(),
    categoryId: DS.attr(),
    products: DS.hasMany('product'),

    makerName: Ember.computed(function () {
        //return this.get('store').peekRecord('maker', this.get('makerId')).get('name');
        return Dicts['maker'].values[this.get('makerId')].name;
    }),

    renderComponent: Ember.computed('categoryId',
        {
            get() {
                switch (`${this.get('categoryId')}`) {
                    case '1': return 'racket-item';
                    case '6': return 'string-item';
                    case '7': return 'grip-item';
                }
                //return `${this.get('categoryId')}` == '6' ? 'string-renderer' : 'grip-props';
            }
        })
});
