﻿import Ember from 'ember';
import DS from 'ember-data';

import Mdl from './mdl';
import Dicts from '../enums/dictionaries';

export default Mdl.extend({
    stringLenghtId: DS.attr(),
    gauge: DS.attr(),
    //mdl: DS.belongsTo('mdl'),
    //stringLenghtName: Ember.computed('stringLenghtId', {
    //    get() {
    //        return this.get('store').peekRecord('stringLenght', `${this.get('stringLenghtId')}`).get('name');
    //    }
    //}),
    stringLenghtName: Ember.computed(function() {
            //return this.get('store').peekRecord('stringLenght', this.get('stringLenghtId')).get('name');
        return Dicts['string-lenght'].values[this.get('stringLenghtId')].name;
    })
});
