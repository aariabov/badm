﻿import DS from 'ember-data';

export default DS.Model.extend({
    min: DS.attr(),
    max: DS.attr()
});
