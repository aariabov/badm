﻿import Ember from 'ember';
import DS from 'ember-data';

import Mdl from './mdl';

export default Mdl.extend({
    gripLength: DS.attr(),
    gripWidth: DS.attr(),
    racketCount: DS.attr(),
    gripMaterialId: DS.attr(),
    gripTypeId: DS.attr(),
    gripTypeName: Ember.computed(function () {
        return this.get('store').peekRecord('gripType', `${this.get('gripTypeId')}`).get('name');
    }),
    gripMaterialName: Ember.computed(function () {
        return this.get('store').peekRecord('gripMaterial', `${this.get('gripMaterialId')}`).get('name');
    })
});
