﻿import DS from 'ember-data';

export default DS.Model.extend({
    actualPrice: DS.attr('number'),
    oldPrice: DS.attr('number'),
    date: DS.attr('date'),
    count: DS.attr('number'),
    countCheckOut: DS.attr('number'),
    littleImg: DS.attr(),
    isVisible: DS.attr('boolean'),
    isHit: DS.attr('boolean'),
    isNew: DS.attr('boolean'),
    isPromo: DS.attr('boolean'),
    isInteres: DS.attr('boolean'),
    isOrder: DS.attr('boolean'),
    model: DS.belongsTo('mdl')
});
