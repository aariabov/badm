﻿import DS from 'ember-data';
import Ember from 'ember';

import Mdl from './mdl';
import Dicts from '../enums/dictionaries';

export default Mdl.extend({
    weight: DS.attr('number'),
    lenght: DS.attr('number'),
    isWithString: DS.attr('boolean'),

    balanceId: DS.attr(),
    flexId: DS.attr(),
    levelId: DS.attr(),
    headMaterialId: DS.attr(),
    shaftMaterialId: DS.attr(),

    balanceName: Ember.computed(function () {
        return Dicts['balance'].values[this.get('balanceId')].name;
    }),
    flexName: Ember.computed(function () {
        return Dicts['flex'].values[this.get('flexId')].name;
    }),
    levelName: Ember.computed(function () {
        return Dicts['level'].values[this.get('levelId')].name;
    }),
    headMaterialName: Ember.computed(function () {
        return Dicts['head-material'].values[this.get('headMaterialId')].name;
    }),
    shaftMaterialName: Ember.computed(function () {
        return Dicts['shaft-material'].values[this.get('shaftMaterialId')].name;
    })
});
