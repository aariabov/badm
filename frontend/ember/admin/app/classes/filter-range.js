﻿import FilterBase from './filter-base';

const FilterRange = FilterBase.extend({
    range: null,
    step: 1,
    min: null,
    max: null,
    start: null,
    userMin: null,
    userMax: null,

    init() {
        this._super();
        //this.set('step', this.get('step') || 1);
        //this.set('selected', []);
    }
});

export default FilterRange;