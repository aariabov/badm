﻿import Ember from 'ember';

const FilterDictValue = Ember.Object.extend({
    id: null,
    name: null,
    count: null,
    isDisabled: null
});

export default FilterDictValue;