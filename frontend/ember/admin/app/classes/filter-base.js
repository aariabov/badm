﻿import Ember from 'ember';

const FilterBase = Ember.Object.extend({
    name: null,
    label: null
});

export default FilterBase;