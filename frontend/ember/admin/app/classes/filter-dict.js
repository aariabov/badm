﻿import FilterBase from './filter-base';

const FilterDict = FilterBase.extend({
    //values: null,

    init() {
        this.set('values', []);
        this.set('selected', []);
    }
});

export default FilterDict;