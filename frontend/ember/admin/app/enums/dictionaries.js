﻿

const dicts = {
//общие
'visibility':
{
    label: 'Видимость',
    values: {
        '1': {
            id: '1',
            name: 'Видимые'
        },
        '2': {
            id: '2',
            name: 'Скрытые'
        }
    }
},
'stock':
{
    label: 'Наличие',
    values: {
        '1': {
            id: '1',
            name: 'В наличии'
        },
        '2': {
            id: '2',
            name: 'Под заказ'
        },
        '3': {
            id: '3',
            name: 'Нет в наличии'
        }
    }
},
'maker':
{
    label: 'Производитель',
    values: {
        '1': {
            id: '1',
            name: 'Babolat'
        },
        '2': {
            id: '2',
            name: 'Yonex'
        },
        '3': {
            id: '3',
            name: 'Victor'
        }
    }
},

// струны
'string-lenght':
{
    label: 'Длина',
    values: {
        '1': {
            id: '1',
            name: '10'
        },
        '2': {
            id: '2',
            name: '200'
        }
    }
},

// ракетки
'balance': {
    label: 'Баланс',
    values: {
        '1': {
            id: '1',
            name: 'В ручку'
        },
        '2': {
            id: '2',
            name: 'Нейтральный'
        },
        '3': {
            id: '3',
            name: 'В голову'
        }
    }
},
'flex': {
    label: 'Жесткость',
    values: {
        '1': {
            id: '1',
            name: 'Экстра мягкая'
        },
        '2': {
            id: '2',
            name: 'Мягкая'
        },
        '3': {
            id: '3',
            name: 'Средняя'
        },
        '4': {
            id: '4',
            name: 'Жесткая'
        },
        '5': {
            id: '5',
            name: 'Экстра жесткая'
        }
    }
},
'level': {
    label: 'Уровень',
    values: {
        '1': {
            id: '1',
            name: 'Профессиональный'
        },
        '2': {
            id: '2',
            name: 'Любительский'
        },
        '3': {
            id: '3',
            name: 'Базовый'
        }
    }
},
'head-material': {
    label: 'Материал стержня',
    values: {
        '1': {
            id: '1',
            name: 'Графит'
        },
        '2': {
            id: '2',
            name: 'Аллюминий'
        },
        '3': {
            id: '3',
            name: 'Сталь'
        }
    }
},
'shaft-material': {
    label: 'Материал обода',
    values: {
        '1': {
            id: '1',
            name: 'Графит'
        },
        '2': {
            id: '2',
            name: 'Аллюминий'
        },
        '3': {
            id: '3',
            name: 'Сталь'
        }
    }
}

}

export default dicts;