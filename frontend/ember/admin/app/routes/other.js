﻿import ProductsBase from './products-base';

export default ProductsBase.extend({
    modelName: 'other',
    header: 'Аксессуары',

    init() {
        this.filtersDict = this.filtersDictBase.slice(0);
        this.filtersRange = this.filtersRangeBase.slice(0);
    }
});
