﻿import Ember from 'ember';
import RSVP from 'rsvp';
import RouteMixin from 'ember-cli-pagination/remote/route-mixin';
import AuthBase from './auth-base';
import FilterDict from '../classes/filter-dict';
import FilterDictValue from '../classes/filter-dict-value';
import FilterRange from '../classes/filter-range';
import Dicts from '../enums/dictionaries';


export default AuthBase.extend(RouteMixin,
{
    templateName: 'products',
    header: "",
    modelName: 'mdl',

    filtersDictBase: [
        FilterDict.create({
            name: 'visibility'
        }),
        FilterDict.create({
            name: 'stock'
        }),
        FilterDict.create({
            name: 'maker'
        })
    ],

    filtersRangeBase: [
        FilterRange.create({
            name: 'price',
            label: 'Цена'
        })
    ],

    model(params) {
        // с сервера возвращается кол-во записей, передаем функцию для вычисления кол-ва страниц
        params.paramMapping = {
            total_pages: [
                "total-count",
                (ops) => {
                    return Math.ceil(ops.rawVal / ops.perPage);
                }
            ],
            perPage: "perPage"
        };

        // чтоб в запросе не отправлялся этот параметр
        params.showVisibilityFilter = undefined;

        // так как значение по умолчанию ['1'], когда очишаем значение в url вылазит visibility= (это не нравится бэкенду, поэтому очищаем)
        if (params.visibility.length === 1 && params.visibility[0] === '') {
            params.visibility = undefined;
        }
        
        return RSVP.hash({
            header: this.header,
            // загрузит фильтры диапазоны (начальные и конечные значения) для текущей модели
            filterRanges: this.get('store').query('filter-range', { 'modelName': this.modelName }),
            filters: this.filtersDict,
            filtersRange: this.filtersRange,
            // загрузит первую порцию данных
            content: this.findPaged(this.modelName, params)
        });
    },

    // ждет, пока разрешаться все обещания
    setupController(controller, model) {
        
        this._super(controller, model);
        
        var meta = model.content.meta;
        if (Object.keys(meta).length === 0) {
            meta = false;
        }

        // создаем фильтры-справочники
        this.filtersDict.forEach(function (filter, i, arr) {
            // заполняем значения фильтров-справочников из закешированных моделей
            if (filter.get('values').length === 0) {
                let model = filter.name;
                //if (model === 'headMaterial' || model === 'shaftMaterial') {
                //    model = 'racketMaterial';
                //}

                filter.label = Dicts[model].label;
                                
                let values = [];
                // перебираем свойства values объекта-справочника и на каждое значение создаем объект FilterDictValue
                // multiselect-checkboxes требует, чтобы это был объект эмберовского класса
                for (let key in Dicts[model].values) {
                    let item = Dicts[model].values[key];
                    let value = FilterDictValue.create({
                        id: item.id,
                        name: item.name
                    });
                    values.addObject(value);
                }
                filter.set('values', values);
            }

            // выделяем нужные значения, если есть параметры в url
            let selected = filter.values.filter(function (value) {
                return controller.get(filter.name).includes(value.id);
            });
            filter.set('selected', selected);

        }.bind(this));

        // инфа о количестве из метаданных
        controller.updateFilterValuesCount();
                
        this.filtersRange.forEach(function (filter, i, arr) {
            // устанавливаем максимальный диапазон
            var range = this.get('store').peekRecord('filter-range', filter.name);
            filter.set('range', {
                min: range.get('min'),
                max: range.get('max')
            });

            let startMin, startMax;
            // устанавливаем начальные значения: если есть queryParam - берем из параметра, иначе - из метаданных
            let queryParamMin = controller.get(filter.name + 'Min');
            if (queryParamMin) {
                //filter.set('min', controller.get(filter.name + 'Min'));
                startMin = queryParamMin;
                filter.set('userMin', queryParamMin);
            } else {
                //filter.set('min', meta ? meta[filter.name].min : 0);
                startMin = meta ? meta[filter.name].min : 0;
            }

            let queryParamMax = controller.get(filter.name + 'Max');
            if (queryParamMax) {
                //filter.set('max', controller.get(filter.name + 'Max'));
                startMax = queryParamMax;
                filter.set('userMax', queryParamMax);
            } else {
                //filter.set('max', meta ? meta[filter.name].max : 0);
                startMax = meta ? meta[filter.name].max : 0;
            }
            
            filter.set('start', [startMin, startMax]);

        }.bind(this));

        // при обновлении модели - обновляем все фильтры        
        model.content.on('contentUpdated', controller.updateAllFilters.bind(controller));
    },

    // чтобы в качестве разделителя айдишников в url использовалась точка
    serializeQueryParam(value, urlKey, defaultValueType) {
        if (defaultValueType === 'array') {
            return value.join('.');
        }

        return this._super(...arguments);
    },

    // чтобы в качестве разделителя айдишников в url использовалась точка
    deserializeQueryParam(value, urlKey, defaultValueType) {
        if (defaultValueType === 'array') {
            return value.split('.');
        }

        return this._super(...arguments);
    },

    resetController(controller, isExiting, transition) {
        if (isExiting) {
            console.log('resetController');
        }
    },
    init() {
        console.log('init base route');
    }
});
