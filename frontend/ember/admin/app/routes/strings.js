﻿import ProductsBase from './products-base';
import FilterDict from '../classes/filter-dict';
import FilterRange from '../classes/filter-range';

export default ProductsBase.extend({
    
    modelName: 'string',
    header: "Струны",

    init() {
      this.filtersDict = this.filtersDictBase.slice(0);
      this.filtersDict.pushObject(
          FilterDict.create({
              name: 'string-lenght'
          })            
      );

      this.filtersRange = this.filtersRangeBase.slice(0);
      this.filtersRange.pushObject(
          FilterRange.create({
              name: 'gauge',
              label: 'Толщина',
              step: 0.01
          })
      );        
    },
    
});
