﻿import ProductsBase from './products-base';
import FilterDict from '../classes/filter-dict';
import FilterRange from '../classes/filter-range';

export default ProductsBase.extend({
    modelName: 'racket',
    header: 'Рекетки',

    init() {
        //console.log('init rackets route');
        this.filtersDict = this.filtersDictBase.slice(0);
        this.filtersDict.pushObjects([
            FilterDict.create({
                name: 'balance'
            }),
            FilterDict.create({
                name: 'flex'
            }),
            FilterDict.create({
                name: 'level'
            }),
            FilterDict.create({
                name: 'head-material'
            }),
            FilterDict.create({
                name: 'shaft-material'
            }),
        ]);
        this.filtersRange = this.filtersRangeBase.slice(0);
        this.filtersRange.pushObject(
            FilterRange.create({
                name: 'weight',
                title: 'Вес'
            })
        );
    }

});