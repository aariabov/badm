﻿import Ember from 'ember';

export default Ember.Route.extend({
    // внедряем сервис, также можно внедрить во все роуты на уровне инициализатора
    loading: Ember.inject.service('loading'),

    actions: {
        // Если вы возвращаете обещание из hooks beforeModel/model/afterModel,
        // и оно не разрешается сразу, на этом маршруте будет запущено событие loading
        // сработает на верхнем уровне, можно переопределять на каждом роуте
        loading(transition, originRoute) {
            // показываем спиннер
            this.get('loading').show();
            transition.promise.finally(function () {
                // после загрузки всех данных прячем спиннер
                this.get('loading').hide();
            }.bind(this));
            return true; // allows the loading template to be shown
        }
    },
});
