﻿import Ember from 'ember';
import ProductBase from './products-base';

export default ProductBase.extend({
    //queryParams: [
    //    "page",
    //    "perPage",
    //    "sort",
    //    "maker",
    //    "balance",
    //    "flex",
    //    "level",
    //    "headMaterial",
    //    "shaftMaterial",
    //    "priceMin",
    //    "priceMax"
    //],
    balance: [],
    flex: [],
    level: [],
    "head-material": [],
    "shaft-material": [],

    init() {
            
        //this.filters = this.filtersBase.slice(0);
        this.queryParams = this.queryParamsBase.concat(
            ["balance",
            "flex",
            "level",
            "head-material",
            "shaft-material"]
        );
        //console.log(this.queryParams);
    }
});
