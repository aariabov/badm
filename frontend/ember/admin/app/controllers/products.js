﻿import Ember from 'ember';

export default Ember.Controller.extend({
    queryParams: ["page", "perPage"],
    //queryParams: test,
    //filters: {
    //    xz: 1
    //},
    page: 1,
    sort: 'name',
    //stringLenght: [],

    init() {
        console.log('init products controller');
        //this.get('queryParams', this.get('test'));
    },

    actions: {
        applyFilters() {
            console.log(this);
            //debugger;
            //this.model.filters.forEach(function (item, i, arr) {
            //    if (item.selected.length > 0) {
            //        var ids = item.selected.map(function (item) {
            //            return +item.get('id');
            //        });
            //        this.set(item.name, ids);
            //        this.get('model.content').setOtherParam(item.name + '[]', ids);
            //        if (this.get('page') === 1) {
            //            Ember.run.once(this.get('model.content'), "pageChanged");
            //        }
            //        else {
            //            this.set('page', 1);
            //        }
            //    }
            //}.bind(this));

            //console.log(stringLenghtIds);
        },

        sortBy(prop) {
            switch (prop) {
                case 'id':
                    this.set('sort', this.get('sort') === 'id' ? '-id' : 'id');
                    break;
                case 'name':
                    this.set('sort', this.get('sort') === 'name' ? '-name' : 'name');
                    break;
            }

            this.get('model.content').setOtherParam('sort', this.get('sort'));
            if (this.get('page') === 1) {
                //this.get('model.content').set("promise", this.get('model.content').fetchContent());
                Ember.run.once(this.get('model.content'), "pageChanged");
            }
            else {
                this.set('page', 1);
            }

        },
    }
});
