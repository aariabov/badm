﻿import Ember from 'ember';
import declOfNum from '../helpers/decl-of-num';

const sortItems = [
    { name: '-Date', label: 'Сначала новые', iconClass: 'fa-sort-amount-desc' },
    { name: 'Date', label: 'Сначала старые', iconClass: 'fa-sort-amount-asc' },
    { name: '-ActualPrice', label: 'Сначала дорогие', iconClass: 'fa-sort-amount-desc' },
    { name: 'ActualPrice', label: 'Сначала недорогие', iconClass: 'fa-sort-amount-asc' }
];

const perPageItems = [1, 2, 3, 4, 5, 10];

export default Ember.Controller.extend({
    // url параметры
    queryParamsBase: [
        "page",
        "perPage",
        "sort",
        "maker",
        "priceMin",
        "priceMax",
        "stock",
        "showVisibilityFilter",
        "visibility"
    ],

    page: 1,
    perPage: 3,
    sort: '-Date',
    maker: [],
    stock: [],
    visibility: ['1'],

    showVisibilityFilter: false, 

    perPageItems: perPageItems,
    perPageSelected: Ember.computed('perPage', function () {
        return this.perPageItems.find(item => item === this.perPage);
    }),

    sortItems: sortItems,
    sortSelected: Ember.computed('sort', function(){
        return this.sortItems.find(item => item.name === this.sort);
    }),

    countInfoText: Ember.computed('model.content.meta', function () {
        var totalCount = this.get('model.content.content.meta.total-count');
        var declModels = declOfNum.compute(totalCount, ['модели', 'моделей', 'моделей']);
        if (this.get('model.content.length') === 1) {
            return `Показана ${(this.page - 1) * this.perPage + 1}-ая из ${totalCount} ${declModels}`;
        } else {
            return `Показано ${(this.page - 1) * this.perPage + 1}-${Math.min(this.page * this.perPage, totalCount)} из ${totalCount} ${declModels}`;
        }
    }),
    
    init() {
        console.log('init BaseProducts controller');
    },

    loadingObserver: Ember.observer('model.content.loading', function () {
        // показываем или прячем экран загрузки
        if (this.get('model.content.loading')) {
            this.get('loading').show();
        } else {
            this.get('loading').hide();
        }
    }),

    // обновляет все фильтры, если модель обновлена, и прячет спиннер
    updateAllFilters: function() {
        this.updateFilterValuesCount();
        this.updateFilterRanges();
    },
    
    // инфа о количестве из метаданных
    updateFilterValuesCount: function () {
        let meta = this.get('model.content.meta');
        if (Object.keys(meta).length === 0) {
            meta = false;
        }

        this.model.filters.forEach(function (filter, i, arr) {
            let modelName = filter.name;
            filter.get('values').forEach((value) => {
                let count = meta ? meta[modelName][value.get('id')] : 0;
                value.set('count', count);
                value.set('isDisabled', count === 0 ? true : false);
            });           
        }.bind(this));
    },

    // изменяет значение фильтров-диапазонов только, если юзер не менял значения
    updateFilterRanges: function () {
        let meta = this.get('model.content.meta');
        if (Object.keys(meta).length > 0) {
            this.model.filtersRange.forEach(function(filter, i, arr) {
                if (!filter.userMin) {
                    //filter.set('min', meta[filter.name].min);
                    //filter.set('max', meta[filter.name].max);
                    let min = meta[filter.name].min;
                    let max = meta[filter.name].max;
                    console.log('set start after update model '+min +' '+max)
                    filter.set('start', [min, max]);
                }
            }.bind(this));
        }
    },

    // обновить только модель (без фильтров)
    updateModel: function () {
        console.log('updateModel');
        if (this.get('page') === 1) {
            // ХАК, чтобы не выполнялось два запроса
            this.set('model.content.lastPage', this.get('model.content.page'));

            this.get('model.content').set("promise", this.get('model.content').fetchContent());
            // не всегда срабатывает, т.к. в пакете стоит проверка, если страница не изменилась - то не делается запрос
            //Ember.run.once(this.get('model.content'), "pageChanged");
        }
        else {
            // сбрасываем страницу и автоматически делается запрос
            this.set('page', 1);
        }
    },

    // анализирует фильтры
    // задает параметры запроса
    // задает url параметры
    updateAll: function () { 
        console.log('updateAll');
        this.model.filtersRange.forEach(function (filter, i, arr) {
            // только если юзер задавал значения
            var minVal = filter.userMin && filter.userMin !== filter.range.min ? filter.userMin : undefined;
            this.set(filter.name + 'Min', minVal);
            this.get('model.content').setOtherParam(filter.name + 'Min', minVal);

            // только если юзер задавал значения
            var maxVal = filter.userMax && filter.userMax !== filter.range.max ? filter.userMax : undefined;
            this.set(filter.name + 'Max', maxVal);
            this.get('model.content').setOtherParam(filter.name + 'Max', maxVal);           
        }.bind(this));

        // для каждого фильтра-справочника, устанавливаем в параметр ид выбранного значения
        this.model.filters.forEach(function (filter, i, arr) {
            var ids = filter.selected.map(function (select) {
                return select.id;
            });

            this.set(filter.name, ids);

            if (ids.length === filter.values.length) {
                this.get('model.content').setOtherParam(filter.name, []);
            } else {
                this.get('model.content').setOtherParam(filter.name, ids);
            }
        }.bind(this));
        
        this.updateModel();
    },

    sortBy(prop) {
        this.set('sort', prop);
        // ХАК, чтобы не выполнялось два запроса
        this.set('model.content.lastPage', this.get('model.content.page'));
        this.get('model.content').setOtherParam('sort', this.get('sort'));
        this.updateModel();
    },

    actions: {

        perPageItemChanged(item) {
            this.set('perPage', item);
            // нельзя выносить this.set('model.content.perPage', this.get('perPage')); наверх, т.к. будет 2 запроса
            // хак, чтобы обойти проверку lastPage
            if (this.get('model.content.lastPage') != this.get('model.content.page')) {
                this.set('model.content.perPage', this.get('perPage'));
            } else {
                this.set('model.content.perPage', this.get('perPage'));
                this.updateModel();
            }
        },

        sort(selectedSort) {
            //console.log(selectedSort);
            this.set('sortSelected', selectedSort);
            this.sortBy(selectedSort.name);
        },

        // при изменении фильтра-диапазона, запоминаем значения и обновляем модель
        rangeSliderChanged(name, val) {
            this.model.filtersRange.forEach(function (filter, i, arr) {
                if (filter.name === name) {
                    filter.set('userMin', val[0]);
                    filter.set('userMax', val[1]);
                }
            });
            this.updateAll();
        },

        // при изменении значения фильтра-справочника, запоминаем и обновляем модель
        checkboxFilterChange: function (filterName, newSelection, value, operation) {            
            var filter = this.model.filters.find((filter)=> {
                return filter.name === filterName;
            });
            Ember.set(filter, "selected", newSelection);
            this.updateAll();          
        },

        // очишаем все фильтры и обновляем модель
        cleanFilters() {
            this.model.filtersRange.forEach(function (filter, i, arr) {
                filter.set('userMin', undefined);
                filter.set('userMax', undefined);
                filter.set('start', [filter.range.min, filter.range.max]);
            }.bind(this));

            this.model.filters.forEach(function (filter, i, arr) {
                if (filter.name === 'visibility' && !this.get('showVisibilityFilter')) {
                    let selected = filter.values.filter(function (value) {
                        return value.id === '1';
                    });
                    filter.set('selected', selected);
                    //Ember.set(filter, "selected", selected);
                } else {
                    filter.set('selected', []);
                }
                
            }.bind(this));

            this.updateAll();
        },

    }
});

/*
логика работы
все работает на основе объектов фильтров: фильтры-справочники (this.model.filters) и фильтры-диапазоны (this.model.filtersRange)
юзер через UI компоненты фильтров может менять значения фильтров
когда обновляем модель, то анализируем фильтры, задаем параметры запроса и url параметры
инфа о количестве продуктов и допустимых диапазонов берется из метаданных
*/