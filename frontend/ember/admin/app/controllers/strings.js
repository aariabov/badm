﻿import Ember from 'ember';
import ProductBase from './products-base';

export default ProductBase.extend({
    //queryParams: ["page", "perPage", "sort", "maker", "stringLenght"],
    'string-lenght': [],
    
    init() {
        //console.log('init strings controller');

        //this.filters = this.filtersBase.slice(0);
        this.queryParams = this.queryParamsBase.concat(
            ["string-lenght", "gaugeMin", "gaugeMax"]
        );
    }
});
