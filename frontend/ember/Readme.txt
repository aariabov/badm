Менял аддоны:
frontend\ember\admin\node_modules\ember-cli-pagination\addon\remote\paged-remote-array.js метод pageChanged: убрал if (lastPage != page) - т.к. если стоим на первой странице и меняем сортировку, то не выполняет запрос. Можно вылечить полным переходом - не очень, все будет заново обновляться и перерисовываться.
исходный метод:
pageChanged: Ember.observer("page", "perPage", function() {
    var page = this.get('page');
    var lastPage = this.get('lastPage');
    if (lastPage != page) {
      this.set('lastPage', page);
      this.set("promise", this.fetchContent());
    }
  }),

мой метод:
pageChanged: Ember.observer("page", "perPage", function() {
    
	var page = this.get('page');
    
	this.set('lastPage', page);
    
	this.set("promise", this.fetchContent());
  
}),

frontend\ember\admin\node_modules\ember-cli-nouislider\addon\components\range-slider.js метод run.schedule: чтобы для нескольких слайдеров использовать одну функцию обработки изменения значения слайдера надо их различать, поэтому задаю каждому слайдеру name и потом параметрами в функцию обработки уведомления отправляются значения слайдера и его имя
исходный метод:
run.schedule('sync', () => {
      this.set('slider', slider);

      sliderEvents.forEach(event => {
        if (!isEmpty(this.get(`on-${event}`))) {
          slider.on(event, () => {
            run(this, function() {
              let val = this.get("slider").get();
              this.sendAction(`on-${event}`, val);
            });
          });
        }
      });

мой метод:
run.schedule('sync', () => {
      this.set('slider', slider);

      sliderEvents.forEach(event => {
        if (!isEmpty(this.get(`on-${event}`))) {
          slider.on(event, () => {
            run(this, function() {
              let val = this.get("slider").get();
              this.sendAction(`on-${event}`, val, this.get("name"));
            });
          });
        }
      });